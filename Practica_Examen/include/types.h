#pragma once

#include <cmath>
#include <stdio.h>
#include <time.h>
#include <vector>

//estructuras para los vectores
struct Vec2 {
	float x, y;
};

struct Point2 {
	float x, y;
};

//Funciones matematicas para usar con vectores
struct VectorsOperations
{
	Vec2 CalcVector(Point2 start, Point2 dest)
	{
		Vec2 v;
		v.x = dest.x - start.x;
		v.y = dest.y - start.y;

		return v;
	}
	float CalcModule(Vec2 v)
	{
		float dist;
		dist = sqrt((v.x * v.x) + (v.y * v.y));
		return dist;
	}
	Vec2 CalcUnitVector(Vec2 v, float module)
	{
		Vec2 aux;
		aux.x = (v.x / module);
		aux.y = (v.y / module);

		return aux;
	}
	Point2 CalcNewPosition(Point2 a, Vec2 b,float step, float speed)
	{
		Point2 e;
		Vec2 v;
		v.x = b.x * speed * step;
		v.y = b.y * speed * step;
		
		e.x = a.x + v.x;
		e.y = a.y + v.y;
		return e;
	}
	float CalcDistance(Point2 a, Point2 b)
	{
		return CalcModule(CalcVector(a, b));
	}
	Vec2 CalcDirection(Point2 pos, Point2 dest)
	{
		Vec2 aux = CalcVector(pos, dest);
		return CalcUnitVector(aux, CalcModule(aux));
	}

	bool ComparePoint2(Point2 a, Point2 b)
	{
		if (a.x == b.x && a.y == b.y) return true;
		else return false;
	}
	Vec2 LeftRotate90(Vec2 a)
	{
		Vec2 b;
		if (a.x > 0 && a.y == 0)
		{
			b.x = 0;
			b.y = -a.x;
		}
		if (a.x == 0 && a.y > 0)
		{
			b.x = a.y;
			b.y = 0;
		}
		if (a.x < 0 && a.y == 0)
		{
			b.x = 0;
			b.y = -a.x;
		}
		if (a.x == 0 && a.y < 0)
		{
			b.x = a.y;
			b.y = 0;
		}
	
		return b;
	}
	Vec2 RightRotate90(Vec2 a)
	{
		Vec2 b;
		if (a.x > 0 && a.y == 0)
		{
			b.x = 0;
			b.y = a.x;
		}
		if (a.x == 0 && a.y > 0)
		{
			b.x = -a.y;
			b.y = 0;
		}
		if (a.x < 0 && a.y == 0)
		{
			b.x = 0;
			b.y = a.x;
		}
		if (a.x == 0 && a.y < 0)
		{
			b.x = -a.y;
			b.y = 0;
		}
		return b;
	}
};