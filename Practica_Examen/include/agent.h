#pragma once

#include "types.h"
#include "gamemanager.h"
#include "messengeragent.h"

class Agent {
public:
	Agent();
	~Agent();

	int id_;
	Point2 pos_;
	int tracking_at_; //agente al que sigo
	int tracking_me_;	//agente que me sigue

	enum StateType {
		kWorking,
		kChasing,
		kFleeing,
		kResting
	};
	StateType state_type_;
	
	enum MoveType {
		kDeterminist,
		kRandom,
		kTrack,
		kPatron,
		kAwaiting
	};
	MoveType move_type_;
	
	enum AgentType {
		kSmall,
		kNormal,
		kHuge
	};
	AgentType agent_type_;
	
	enum InstructionType {
		kForward,
		kLeft,
		kRight,
		kStop
	};
	InstructionType instruction_type_;

	void Init(int id, AgentType ag, float x = 0, float y = 0);
	void Update(int step);
	void Mind(int step);
	void Body(int step);
	void Draw();

	//metodo que es llamado cuando quieren mandarme un mensaje
	bool SM_Agent(int id_agent_to_pursuit);

private:

	GameManager *gm_ = GameManager::Instance();
	
	VectorsOperations	vo;
	float aux_dist_, dist_;
	int nearest_id_, chase_dinstance_, flee_distance_,rest_distance_;
	float resting_time_;
	bool reached_;
	Point2 dest_, start_;
	Vec2 direction_;
	float radio_patrol_;
	float speed_;
	
	//modificar este valor en caso de modificar el nuemero de puntos
	//de navegacion.
	static const int num_nav_points_ = 3;
	Point2 nav_points_[num_nav_points_];
	
	int nav_point_reached_,patron_point_reached_;
	bool patron_choose_;
	double current_time_, time_;
	float travel_speed_;
	int time_elapsed_;
	bool time_reached_;
	int current_unit_patron_, length_unit_patron_;

	std::vector<InstructionType> patron_;
	MessengerAgent *ma_;
	
	//Patrones de movimiento	
	InstructionType patron_1_[12] =
	{ kStop,kForward,kForward,kRight,
		kLeft,kRight,kRight,kForward,
		kRight,kLeft,kRight,kStop };
	InstructionType patron_2_[15] =
	{ kStop,kForward,kStop,kLeft,
		kLeft,kForward,kLeft,kStop,
		kForward,kLeft,kForward,
		kLeft,kStop,kLeft,kStop };
	InstructionType patron_3_[19] =
	{ kStop,kForward,kForward,
		kForward,kRight,kRight,
		kForward,kStop,kLeft,
		kForward,kRight,kRight,
		kForward,kLeft,kForward,
		kRight,kRight,kForward,kStop };
											 
	//metodos de mind
	void Working();
	void Fleeing();
	void Resting();
	void Chasing();
	//metodos de body
  void DeterministMovment(int step);
	void RandomMovment(int step);
	void TrackMovment(int step);
	void PatronMovment(int step);
	void AwaitingMovment(int step);
	//metodos auxiliares
	void ChoosePatron();
	void ResetDirection();
	


protected:

};




