#include "types.h"
#include "gamemanager.h"
#include "agent.h"
#include "messengeragent.h"

#include <stdlib.h>

namespace ESAT {
	
	///////Bucle del START///////

	void Imput()
	{
		GameManager* gm_ = GameManager::Instance();
		//cierro la ventana/programa con la tecla ESC
		if (ESAT::IsSpecialKeyPressed(kSpecialKey_Escape) || 
				!ESAT::WindowIsOpened())
		{
			gm_->window_closed = true;

		}
	}
	void Update(int step)
	{
		GameManager* gm_ = GameManager::Instance();
		double s = Time();
		double e = 0;
		int d = 0;
		do
		{
			//llamo al update de los agentes
			//actualizo tantos agentes como puedo dependiendo del
			//step que me quede
			gm_->agent_.at(gm_->agent_vector_position_)->Update(step);
			
			//printf("agente: %d\n", gm_->agent_vector_position_);
			
			gm_->agent_vector_position_++;
			if (gm_->agent_vector_position_ == gm_->agent_.size())
			{
				gm_->agent_vector_position_ = 0;
			}
			e = Time() - s;
			d++;
		} while (e < gm_->real_time_to_update_ && d < gm_->agent_.size());
		
			//printf("           UPDATE: %f\n", e);
		
		//Update del MessengerAgent
		gm_->messengeragent_->Update(step);
	}
	void Draw()
	{
		GameManager* gm_ = GameManager::Instance();
		ESAT::DrawBegin();
		ESAT::DrawClear(0, 0, 0);
		gm_->Draw_time = Time();
	
		//llamo al draw de los agentes
		for (int i = 0; i < gm_->agent_.size(); i++)
		{
			gm_->agent_.at(i)->Draw();
		}
		gm_->Draw_time = Time() - gm_->Draw_time;
		
		//printf("Draw Time: %f\n", Draw_time);
		
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	///////Funciones principales////////////

	void Init()
	{
		GameManager* gm_ = GameManager::Instance();
		int aux = 0;

 /////////////////////TEST/////////////////////
///////////////////////////////////////////////

 //Inicializacion de todos los agentes

 //si ponemos #if 1 tendremos que decir en el 
//gamemanager cuantos agentes queremos de cada tipo
#if 1
		
		//inicializo el MessengerAgent
		MessengerAgent *ma = new MessengerAgent;
		ma->Init(0);
		gm_->messengeragent_ = ma;
		
		
		int small = gm_->num_small;
		int normal = small + gm_->num_normal;
		int huge = normal + gm_->num_huge;
		
		//inicializo los agentes Small
		for (int i = 0; i < small; i++)
		{
			Agent *a = new Agent;// = new Agent();
			a->Init(i, Agent::kSmall);
			gm_->agent_.push_back(a);
			
		}
		//inicializo los agentes Normal
		for (int i = small; i < normal; i++)
		{
			Agent *a = new Agent;// = new Agent();
			a->Init(i, Agent::kNormal);
			gm_->agent_.push_back(a);
			gm_->messengeragent_->
				RegisterAgent(i, MessengerAgent::kNormalPursuit);
		}
		//inicializo los agentes Huge
		for (int i = normal; i < huge; i++)
		{
			Agent *a = new Agent;// = new Agent();
			a->Init(i, Agent::kHuge);
			gm_->agent_.push_back(a);
			gm_->messengeragent_->
				RegisterAgent(i, MessengerAgent::kHugePursuit);
		}
		
//si ponemos #if 0 (arriba) podemos poner hasta tres agentes deonde 
//queramos, para poder hacer pruebas.
#else 
		MessengerAgent *ma = new MessengerAgent;
		ma->Init(0);
		gm_->messengeragent_ = ma;
		
		Agent *a = new Agent;
		Agent *b = new Agent;
		Agent *c = new Agent;

		a->Init(0, Agent::kSmall,500,600);
		b->Init(1, Agent::kNormal,450,600);
		c->Init(2, Agent::kNormal, 550, 600);

		gm_->agent_.push_back(a);
		gm_->agent_.push_back(b);
		gm_->agent_.push_back(c);

		gm_->messengeragent_->
			RegisterAgent(1, MessengerAgent::kNormalPursuit);
		gm_->messengeragent_->
			RegisterAgent(2, MessengerAgent::kNormalPursuit);
		
#endif		
////////////////////END-TEST///////////////////
///////////////////////////////////////////////
	}
	void Start()
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->ct = Time();
		srand(time(NULL));
		double start_time_to_update_ = 0;
		//bucle principal  
		while (!gm_->window_closed)
		{
			Imput();
			
			//calculo el tiempo que tardo en hacer el imput y 
			//el draw para saber cuanto tiempo me queda para
			//el Update
			gm_->real_time_to_update_ = gm_->IA_fixedStep - 
																 (Time() - start_time_to_update_);
			
			//printf("Time for Update: %f\n", gm_->real_time_to_update_);
			
			float accutime = Time() - gm_->ct;
			
			while (accutime >= gm_->IA_fixedStep)
			{
				Update(gm_->IA_fixedStep);
				gm_->ct += gm_->IA_fixedStep;
				accutime = Time() - gm_->ct;
			}
			start_time_to_update_ = Time();
			
			Draw();
		}
	}
	void ShutDown()
	{
		GameManager* gm_ = GameManager::Instance();
	}

	//////////////Main//////////////
	int ESAT::main(int argc, char **argv)
	{
		ESAT::WindowInit(1000,800);
		GameManager* gm_ = GameManager::Instance();
		gm_->Init();
				
		Init(); 
		Start();
		ShutDown();
			
		ESAT::WindowDestroy();
		return 0;
	}
}//end namespace ESAT