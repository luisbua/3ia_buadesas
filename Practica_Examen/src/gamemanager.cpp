#include "gamemanager.h"

GameManager::GameManager() {

}
GameManager::~GameManager() {

}
void GameManager::Init() {
	
	s_small = ESAT::SpriteFromFile("../../data/small.png");
	s_normal = ESAT::SpriteFromFile("../../data/medium.png");
	s_huge = ESAT::SpriteFromFile("../../data/huge.png");

	agent_vector_position_ = 0;
	real_time_to_update_ = 0;
	
	window_closed = false;
}
GameManager* GameManager::instance_;