solution "3IA_Final"

platforms {"x64", "x32"}
configurations {"Debug", "Release"}

location "build/3IA_Final"
  project "3IA_Final"
    language "C++"
    kind "consoleApp"

    files {"include/**.h","src/**.cpp"}
    includedirs {"include"}
    
    libdirs{"extern/lib/ESAT_SDK"}    
    includedirs{"extern/include"}

    configuration "Debug"
      flags {"Symbols"}
		links{ "ESAT_d"
		}
    configuration "Release"
      flags {"OptimizeSize"}
		links{ "ESAT"
		}
    configuration "windows"
      targetdir   "build/bin/windows"
      links { "imm32",
              "oleaut32",
              "winmm",
              "version",
              "OpenGL32"
            }

