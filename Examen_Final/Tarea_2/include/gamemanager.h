#pragma once

#include "ESAT/input.h"
#include "ESAT/window.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "ESAT/sprite.h"

#include <stdio.h>
#include <time.h>
#include <vector>
#include <memory>

#include "types.h"
#include "map.h"
#include "cost_map.h"




//#include "agent.h"
class Agent;
class MessengerAgent;
class CostMap;


class GameManager
{
private:
	static GameManager *instance_;
	GameManager();
	~GameManager();
public:
	
	static GameManager* Instance() {
		if (!instance_)
		{
			instance_ = new GameManager();
		}
		
		return instance_;
	}
	void Init();
	
			
	//ancho y alto de la ventana
	const int width = 960;
	const int height = 704;

	bool window_closed;
	 
	//step in ms, 40 ms = 25 fps
	const int IA_fixedStep = 40; 
	
	double ct;
	float Draw_time;
	
	int soldier_vector_position_, hostage_vector_position_, guard_vector_position_;
	float real_time_to_update_;
	int scale_;
	float dist_to_use_door_;
	bool alarm_;
	int alarm_off_;
	float alarm_time_;
	VectorsOperations	vo;

	void FillRooms();
	bool CalcPathToFoe(Point_2 orig, Point_2 dest);

	//Soldiers Vector
	std::vector<Agent*> soldier_;
	//Hostages Vector
	std::vector<Agent*> hostage_;
	//Guards Vector
	std::vector<Agent*> guard_;

	//agents vector
	std::vector<Agent*> agent_;
	

	//MessengerAgent
	MessengerAgent *messengeragent_;

	//Map an CostMap
	Map map;
	CostMap cost_map;

	//Determinist Paths
	std::vector<Point_2> go_to_work_;
	std::vector<Point_2> working_;
	std::vector<Point_2> go_to_rest_;
	std::vector<Point_2> from_B_to_yard_;
	std::vector<Point_2> base_B_to_door_A_;
	std::vector<Point_2> scape_for_A_;


	//Interesting Points
	Point_2 door_A_;
	Point_2 door_B_;
	Point_2 base_A_;
	Point_2 base_B_;
	Point_2 ext_door_A_;
	Point_2 int_door_A_;
	Point_2 int_door_B_;
	Point_2 ext_door_B_;
	Point_2 yard_door_right_;
	Point_2 yard_door_left_;
	Point_2 scape_left_;
	Point_2 scape_right_;

	//Guards
	struct Guard {
		Point_2 guard_start_;
		int start_room;
	};
	Guard guards_[10];

	struct Room {
		int id_;
		Point_2 upper_square_;
		Point_2 lower_square_;
		std::vector<Point_2> doors_;

	};
	Room room_[28];

	//sprites para pintar
	ESAT::SpriteHandle s_soldier_;
	ESAT::SpriteHandle s_hostage_;
	ESAT::SpriteHandle s_guard_;
	ESAT::SpriteHandle s_open_;
	ESAT::SpriteHandle s_close_;
	ESAT::SpriteHandle s_alarm_;
	
	//Arrow variables
	ESAT::SpriteHandle s_arrow_;
	float MousePositionX;
	float MousePositionY;
	
	//adyacence matrix
	int adyacence_matrix_[784] = {
	//										1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2
	//0	1	2	3	4	5	6	7	8	9	0	1	2	3	4	5	6	7	8	9	0	1	2	3	4	5	6	7
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,//0
		0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,//1
		0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//2
		0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//3
		0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//4
		0,0,1,0,1,0,0,0,0,1,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,//5
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,//6
		0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//7
		0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//8
		0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//9
		0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//10
		0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//11
		0,0,1,0,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,//12
		0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,//13
		0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//14
		0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//15
		1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,//16
		0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,//17
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,//18
		0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,//19
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,//20
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,//21
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,//22
		0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,//23
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,//24
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,//25
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,//26
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,//27

		
	};
	
};
