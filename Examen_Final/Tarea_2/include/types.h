#pragma once

#include <cmath>

struct Vec_2 {
	float x, y;
};

struct Point_2 {
	float x, y;
};

//Funciones matematicas para usar con vectores
struct VectorsOperations
{
	Vec_2 CalcVector(Point_2 start, Point_2 dest)
	{
		Vec_2 v;
		v.x = dest.x - start.x;
		v.y = dest.y - start.y;

		return v;
	}
	float CalcModule(Vec_2 v)
	{
		float dist;
		dist = sqrt((v.x * v.x) + (v.y * v.y));
		return dist;
	}
	Vec_2 CalcUnitVector(Vec_2 v, float module)
	{
		Vec_2 aux;
		aux.x = (v.x / module);
		aux.y = (v.y / module);

		return aux;
	}
	Point_2 CalcNewPosition(Point_2 a, Vec_2 b, float step, float speed)
	{
		Point_2 e;
		Vec_2 v;
		v.x = b.x * speed * step;
		v.y = b.y * speed * step;

		e.x = a.x + v.x;
		e.y = a.y + v.y;
		return e;
	}
	float CalcDistance(Point_2 a, Point_2 b)
	{
		return CalcModule(CalcVector(a, b));
	}
	Vec_2 CalcDirection(Point_2 pos, Point_2 dest)
	{
		Vec_2 aux = CalcVector(pos, dest);
		return CalcUnitVector(aux, CalcModule(aux));
	}

	bool ComparePoint_2(Point_2 a, Point_2 b)
	{
		if (a.x == b.x && a.y == b.y) return true;
		else return false;
	}
	Vec_2 LeftRotate90(Vec_2 a)
	{
		Vec_2 b;
		if (a.x > 0 && a.y == 0)
		{
			b.x = 0;
			b.y = -a.x;
		}
		if (a.x == 0 && a.y > 0)
		{
			b.x = a.y;
			b.y = 0;
		}
		if (a.x < 0 && a.y == 0)
		{
			b.x = 0;
			b.y = -a.x;
		}
		if (a.x == 0 && a.y < 0)
		{
			b.x = a.y;
			b.y = 0;
		}

		return b;
	}
	Vec_2 RightRotate90(Vec_2 a)
	{
		Vec_2 b;
		if (a.x > 0 && a.y == 0)
		{
			b.x = 0;
			b.y = a.x;
		}
		if (a.x == 0 && a.y > 0)
		{
			b.x = -a.y;
			b.y = 0;
		}
		if (a.x < 0 && a.y == 0)
		{
			b.x = 0;
			b.y = a.x;
		}
		if (a.x == 0 && a.y < 0)
		{
			b.x = -a.y;
			b.y = 0;
		}
		return b;
	}

};