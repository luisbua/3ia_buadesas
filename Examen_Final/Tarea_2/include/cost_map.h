
// project A Star
// cost_map.h
// author: Toni Barella
// Artificial Intelligence - 3VGP
// ESAT 2016/2017

#pragma once

#include "ESAT_SDK/sprite.h"

#include "map.h"
#include "types.h"

#include <vector>
#include <cstdio>

using namespace ESAT;


typedef float Cost;

// Use one of the next declarations
typedef struct cell_s {
	Vec_2 position_;
	bool is_walkable_;
	Cost cost_;
} Cell;

class CostMap
{
public:
	
	/**
	* @brief Load Cost Maps from BMP or TXT files
	*/
	// Cost map can be a text file or an image file
	bool Load(const char *path);
	
	CostMap();

	// Todo: Add setters / getters
	bool isWalkable(Point_2 position,bool callexternal = false);
	// Prints the cells of cost_map
	void Print(); 
	// Draws the cost map's image if available
	void Draw(int x = 0, int y = 0); 

	//To Open or Close Door
	void UseDoor(char door,char* state);

	std::vector<Cell> cost_map_;
	int width_;
	int height_;
	int scale_;
	bool door_A_open_ = false;
	bool door_B_open_ = false;

	Point_2 door_A_costmap_[4] =
	{ 102,32,
		102,33,
		102,34,
		102,35 };
	Point_2 door_B_costmap_[3] =
	{
		18,27,
		19,27,
		20,27
	};

private:
	 SpriteHandle map_handle_;
	 FILE *pFile;
	 char buffer[1];
	 
	 bool LoadTXT(const char *path);
	 bool LoadBMP(const char *path);
	 void CalcCellCostBMP();
};


