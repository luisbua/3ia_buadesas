#pragma once

#include "types.h"
#include "gamemanager.h"
#include "messengeragent.h"
#include "path.h"
#include "astar.h"

class Agent {
public:
	Agent();
	~Agent();

	int id_;
	Point_2 pos_;
	int tracking_at_; //agente al que sigo
	int tracking_me_;	//agente que me sigue
	
	enum StateType {
		kWorking,
		kWalkingTo,
		kGoToWorking,
		kGoToRest,
		kChasing,
		kFleeing,
		kResting,
		kAwaitng,
		kPatroll,
		kUseDoor,
		kSearchDoor,
		kScape
	};
	StateType state_type_;
	
	enum MoveType {
		kDeterminist,
		kRandom,
		kTrack,
		kPatron,
		kAwaitingMove
	};
	MoveType move_type_;
	
	enum AgentType {
		kSoldier,
		kGuard,
		kHostage
	};
	AgentType agent_type_;
	
	enum InstructionType {
		kForward,
		kLeft,
		kRight,
		kStop
	};
	InstructionType instruction_type_;

	void Init(int id, AgentType ag, float x = 0, float y = 0, int room = 0);
	void Update(int step);
	void Mind(int step);
	void Body(int step);
	void Draw();

	//metodo que es llamado cuando quieren mandarme un mensaje
	bool SM_Agent(int id_agent_to_pursuit);

private:

	GameManager *gm_ = GameManager::Instance();
	
	VectorsOperations	vo;
	float aux_dist_, dist_;
	int nearest_id_, chase_dinstance_, flee_distance_,rest_distance_;
	float resting_time_,patrolling_time_;
	bool reached_, astar_calculated;
	bool go_to_work_, working_, go_to_rest_;
	Point_2 dest_, start_,random_start_;
	Vec_2 direction_;
	float random_width_,random_height_;
	float speed_;
	
	
	Path nav_points_;
	AStar astar_;
	
	int nav_point_reached_,patron_point_reached_;
	bool patron_choose_;
	double current_time_, time_;
	float travel_speed_;
	int time_elapsed_;
	bool time_reached_;
	int current_unit_patron_, length_unit_patron_;
	int time_working_;
	bool go_A_B_;
	bool go_B_A_;
	bool inside_;
	int in_room_;
	bool patrolling_;
	bool closing_door_;
	int adyacent_tmp_;
	bool fleeing_;
	bool scape_to_A_;
	bool awaiting_;
	bool chasign_soldier_;

	std::vector<int> adyacent_rooms_;
	std::vector<int> adyacent_rooms_tmp_;

	std::vector<InstructionType> patron_;
	MessengerAgent *ma_;
	
	//Patrones de movimiento (no usados)	
	InstructionType patron_1_[12] =
	{ kStop,kForward,kForward,kRight,
		kLeft,kRight,kRight,kForward,
		kRight,kLeft,kRight,kStop };
	InstructionType patron_2_[15] =
	{ kStop,kForward,kStop,kLeft,
		kLeft,kForward,kLeft,kStop,
		kForward,kLeft,kForward,
		kLeft,kStop,kLeft,kStop };
	InstructionType patron_3_[19] =
	{ kStop,kForward,kForward,
		kForward,kRight,kRight,
		kForward,kStop,kLeft,
		kForward,kRight,kRight,
		kForward,kLeft,kForward,
		kRight,kRight,kForward,kStop };
											 
	//metodos de mind
	void Working();
	void WalkingTo();
	void GoToWorking();
	void GoToRest();
	void Fleeing();
	void Resting();
	void Chasing();
	void Awaiting();
	void Patrolling();
	void UseDoor();
	void SearchDoor();
	void Scape();
	//metodos de body
  void DeterministMovment(int step);
	void RandomMovment(int step);
	void TrackMovment(int step);
	void PatronMovment(int step);
	void AwaitingMovment(int step);
	//metodos auxiliares
	void ChoosePatron();
	void ResetDirection();
	void PathToNavPoints();
	void OpenDoor();	
	void CloseDoor();
	int SelecAdyacent(int in_room);
	void GoToNextRoom(int room);
	bool CheckInside();
	int InRoom();


protected:

};




