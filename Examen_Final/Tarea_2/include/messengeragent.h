#pragma once
#include "types.h"
#include "gamemanager.h"
#include "agent.h"


class MessengerAgent {
public:
	MessengerAgent();
	~MessengerAgent();
	 
	int id_;
	enum StateType {
		kAwaitingMessage
	};
	StateType state_type_;

	enum 	PursuitKind {
		kNormalPursuit,
		kHugePursuit
	};

	void Init(int id);
	void Update(int step);
	void Mind(int step);
	void Body(int step);
	void Draw();
  //metodo que es llamado cuando quieren enviarme un mensaje
	bool SM_Messenger(int id_agent, int id_agent_to_pursuit, 
									 char* message, PursuitKind pk);
	//metodo que es llamado cuando quieren registrarse en la cola
	//correspondiente (normal o huge)
	void RegisterAgent(int id_agent, PursuitKind pk);

private:
	
	//colas de broadcast, almaceno solo el id del agente
	std::vector<int> normal_pursuit_;
	std::vector<int> huge_pursuit_;
	
	//estado unico del agente
	void AwaitingMessage();
 
protected:
};