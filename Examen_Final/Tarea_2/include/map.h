#pragma once

#include "ESAT_SDK\sprite.h"

using namespace ESAT;

	class Map {

	public:
		
		Map();
		/**
		* @brief Load Cost Maps from BMP files
		*/
		void Load(const char *path);
		/**
		* @brief Draw map at coordinates (0,0)
		*/
		void Draw();
			
		SpriteHandle map;
		int width = 100;
		int height = 100;

};
