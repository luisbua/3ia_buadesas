
// project A Star
// path.h
// author: Toni Barella
// Artificial Intelligence - 3VGP
// ESAT 2016/2017

#ifndef __PATH__
#define __PATH__

#include "types.h"
#include "ESAT_SDK\sprite.h"

#include <vector>

	class Path{
	public:

		//Variables
		std::vector<Point_2> path_;
		int current_point_ = 0;
		bool end_path_ = false;
				
		//Functions
		/**
		* @brief Returns size of path_
		*/
		int Length();

		/**
		* @brief Add new popstion node at begin of path_
		*/
		bool Add(Point_2 position);
		
		/**
		* @brief Return next point at path
		*/
		Point_2 NextPoint();

		void Print(int scale); // Prints the path's positions

	private:
		ESAT::SpriteHandle soldier_;
		ESAT::SpriteHandle start_;
		ESAT::SpriteHandle end_;
		
	};
 
#endif // __PATH__