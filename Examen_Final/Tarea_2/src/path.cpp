#include "path.h"

bool Path::Add(Point_2 position) {

	path_.insert(path_.begin(),position);
	return true;
}

int Path::Length() {
	return path_.size();
}
Point_2 Path::NextPoint() {
	int tam = path_.size();
	if (current_point_ < tam - 1) {
		current_point_++;
		end_path_ = false;
		return path_.at(current_point_);
	}
	else {
		end_path_ = true;
		return path_.back();
	}
}

void Path::Print(int scale) {

	//Sprites to draw
	soldier_ = ESAT::SpriteFromFile("../../data/bua.png");
	start_ = ESAT::SpriteFromFile("../../data/node_green.png");
	end_ = ESAT::SpriteFromFile("../../data/node.png");
	
	for(int i = 0; i < path_.size(); i++)
	{
		//Sprite to draw path
		ESAT::DrawSprite(soldier_, path_.at(i).x * scale,
															path_.at(i).y * scale);
		//Sprite to Draw Start Node
		ESAT::DrawSprite(start_, path_.front().x * scale,
			path_.front().y * scale);
		//Sprite to Draw Goal Node
		ESAT::DrawSprite(end_, path_.back().x * scale,
		path_.back().y * scale);
	}
	
}