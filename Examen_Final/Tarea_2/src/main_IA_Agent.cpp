#include "types.h"
#include "gamemanager.h"
#include "agent.h"
#include "messengeragent.h"

#include <stdlib.h>
	/*
	*/
namespace ESAT {
	
	///////Bucle del START///////

	void Imput()
	{
		GameManager* gm_ = GameManager::Instance();
		//cierro la ventana/programa con la tecla ESC
		if (ESAT::IsSpecialKeyPressed(kSpecialKey_Escape) || 
				!ESAT::WindowIsOpened())
		{
			gm_->window_closed = true;

		}
		gm_->MousePositionX = round(ESAT::MousePositionX());
		gm_->MousePositionY = round(ESAT::MousePositionY());
		
		if (ESAT::MouseButtonDown(0)) {

		gm_->cost_map.UseDoor('B', "Close");
			printf("\nX: %3.0f Y: %3.0f", gm_->MousePositionX, gm_->MousePositionY);
		}
		if (ESAT::MouseButtonDown(1)) {

			gm_->cost_map.UseDoor('A', "Close");
			printf("\nX: %3.0f Y: %3.0f", gm_->MousePositionX, gm_->MousePositionY);
		}
	}
	void Update(int step)
	{
		GameManager* gm_ = GameManager::Instance();
		double start_time = Time();
		double elapsed_time = 0;
		int count = 0;
		bool soldier_end = false;
		bool hostage_end = false;
		bool guard_end = false;

		//Update del MessengerAgent
		gm_->messengeragent_->Update(step);

		do
		{
			//actualizo tantos agentes como puedo dependiendo del
			//step que me quede

			//llamo al update de los soldiers
			if (gm_->soldier_.size() > 0 && !soldier_end) {
				gm_->soldier_.at(gm_->soldier_vector_position_)->Update(step);

				gm_->soldier_vector_position_++;
				if (gm_->soldier_vector_position_ == gm_->soldier_.size())
				{
					gm_->soldier_vector_position_ = 0;
					soldier_end = true;
				}
			}
			//llamo al update de los hostages
			if (gm_->hostage_.size() > 0 && !hostage_end) {
				gm_->hostage_.at(gm_->hostage_vector_position_)->Update(step);

				gm_->hostage_vector_position_++;
				if (gm_->hostage_vector_position_ == gm_->hostage_.size())
				{
					gm_->hostage_vector_position_ = 0;
					hostage_end = true;
				}
			}
			//llamo al update de los guards
			if (gm_->guard_.size() > 0 && !guard_end) {
				gm_->guard_.at(gm_->guard_vector_position_)->Update(step);

				gm_->guard_vector_position_++;
				if (gm_->guard_vector_position_ == gm_->guard_.size())
				{
					gm_->guard_vector_position_ = 0;
					guard_end = true;
				}
			}


			elapsed_time = Time() - start_time;
			count++;
		} while (elapsed_time < gm_->real_time_to_update_);
		
			//printf("           UPDATE: %f\n", e);
		
		
	}
	void Draw()
	{
		GameManager* gm_ = GameManager::Instance();
		ESAT::DrawBegin();
		ESAT::DrawClear(0, 0, 0);
		gm_->Draw_time = Time();
		gm_->map.Draw();

		//Draw arrow
		ESAT::DrawSprite(gm_->s_arrow_, gm_->MousePositionX, gm_->MousePositionY);
		
		//Call Guards Draw
		for (int i = 0; i < gm_->guard_.size(); i++)
		{
			gm_->guard_.at(i)->Draw();
		}
		//Call Soldiers Draw
		for (int i = 0; i < gm_->soldier_.size(); i++)
		{
			gm_->soldier_.at(i)->Draw();
		}
		//Call Hostages Draw
		for (int i = 0; i < gm_->hostage_.size(); i++)
		{
			gm_->hostage_.at(i)->Draw();
		}
		gm_->Draw_time = Time() - gm_->Draw_time;
		
		//Draw A Door State
			Point_2 pos_;
			for (int i = 0; i < 4; i++)
			{
				pos_ = gm_->cost_map.door_A_costmap_[i];
				if (gm_->cost_map.cost_map_[4302].is_walkable_) {
					ESAT::DrawSprite(gm_->s_open_, pos_.x * gm_->scale_, pos_.y * gm_->scale_);
				}
				else {
					ESAT::DrawSprite(gm_->s_close_, pos_.x * gm_->scale_, pos_.y * gm_->scale_);
				}
			}
			
		//Draw B Door State
			for (int i = 0; i < 3; i++)
			{
				pos_ = gm_->cost_map.door_B_costmap_[i];
				if (gm_->cost_map.cost_map_[3259].is_walkable_) {
					ESAT::DrawSprite(gm_->s_open_, pos_.x * gm_->scale_, pos_.y * gm_->scale_);
				}
				else {
					ESAT::DrawSprite(gm_->s_close_, pos_.x * gm_->scale_, pos_.y * gm_->scale_);
				}
			}

		 //Draw Alarm Icon
			if (gm_->alarm_) {
				ESAT::DrawSprite(gm_->s_alarm_,30,100);
			}
		//printf("Draw Time: %f\n", Draw_time);
				
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	///////Funciones principales////////////

	void Init()
	{
		GameManager* gm_ = GameManager::Instance();
		int aux = 0;

 /////////////////////TEST/////////////////////
///////////////////////////////////////////////

  //Inicializacion de todos los agentes
		
		///////SOLDIERS////////
		for (int i = 0; i < 10; i++)
		{
			gm_->soldier_.push_back(new Agent);
			gm_->soldier_.at(i)->Init(i, Agent::kSoldier, 50 + (i*10), 650 + (i*3));
			
			/////Usar esta linea para que los soldados empiezen encima del castillo
			/////y sean mas rapidas las pruebas (comentar la de arriba)
			//gm_->soldier_.at(i)->Init(i, Agent::kSoldier,490 + (i * 10), 20 );
			
		}
		//////HOSTAGES////////
		int num_hostages = 10;//no usar mas de 10
		for (int i = 0; i < num_hostages; i++)
		{
			gm_->hostage_.push_back(new Agent);
			if (i < num_hostages/2 )
				gm_->hostage_.at(i)->Init(i, Agent::kHostage, 666 + i, 510 + i);
			else
				gm_->hostage_.at(i)->Init(i, Agent::kHostage, 430 + i * 20, 300 + i);
		}
		///////GUARDS/////////
		for (int i = 0; i < 10; i++)
		{
			gm_->guard_.push_back(new Agent);
			gm_->guard_.at(i)->Init(i, Agent::kGuard,
									gm_->guards_[i].guard_start_.x, 
									gm_->guards_[i].guard_start_.y,
									gm_->guards_[i].start_room);

		}
	
		MessengerAgent *ma = new MessengerAgent;
		ma->Init(0);
		gm_->messengeragent_ = ma;

		gm_->messengeragent_->
			RegisterAgent(1, MessengerAgent::kNormalPursuit);
		gm_->messengeragent_->
			RegisterAgent(2, MessengerAgent::kNormalPursuit);
		
////////////////////END-TEST///////////////////
///////////////////////////////////////////////
	}
	void Start()
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->ct = Time();
		srand(time(NULL));
		double start_time_to_update_ = 0;
		//bucle principal  
		while (!gm_->window_closed)
		{
			Imput();
			
			//calculo el tiempo que tardo en hacer el imput y 
			//el draw para saber cuanto tiempo me queda para
			//el Update
			gm_->real_time_to_update_ = gm_->IA_fixedStep - 
																 (Time() - start_time_to_update_);
			
			//printf("Time for Update: %f\n", gm_->real_time_to_update_);
			
			float accutime = Time() - gm_->ct;
			
			while (accutime >= gm_->IA_fixedStep)
			{
				Update(gm_->IA_fixedStep);
				gm_->ct += gm_->IA_fixedStep;
				accutime = Time() - gm_->ct;
			}
			start_time_to_update_ = Time();
			
			Draw();
		}
	}
	void ShutDown()
	{
		GameManager* gm_ = GameManager::Instance();
	}

	//////////////Main//////////////
	int ESAT::main(int argc, char **argv)
	{
		ESAT::WindowInit(960,704);
		GameManager* gm_ = GameManager::Instance();
		gm_->Init();
				
		Init(); 
		Start();
		ShutDown();
			
		ESAT::WindowDestroy();
		return 0;
	}
}//end namespace ESAT