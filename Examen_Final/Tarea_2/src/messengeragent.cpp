#include "messengeragent.h"

MessengerAgent::MessengerAgent()
{
}
MessengerAgent::~MessengerAgent()
{
}

void MessengerAgent::Init(int id)
{
	GameManager* gm_ = GameManager::Instance();
	id_ = id;
	state_type_ = kAwaitingMessage;
}
/////////////UPDATE///////////////
void MessengerAgent::Update(int step)
{
	Mind(step);
	Body(step);
}
void MessengerAgent::Mind(int step)
{
	switch (state_type_)
	{
	case MessengerAgent::kAwaitingMessage: AwaitingMessage(); break;
	
	default: printf("State Type Wrong\n");	break;
	}
}
void MessengerAgent::Body(int step)
{		
	//No hay cuerpo
}
/////////////DRAW///////////////
void MessengerAgent::Draw()
{
	//No se pinta nada
}
/////////////MIND///////////////
void MessengerAgent::AwaitingMessage()
{
	//estado unico del agente
}
/////////OTHER FUNCTIONS//////////
/*
*/
bool MessengerAgent::SM_Messenger(int id_agent, int id_agent_to_pursuit,
  															 char* message, PursuitKind pk)
{
	GameManager* gm_ = GameManager::Instance();
	bool answer = false;
	//evaluo el tipo de mensaje (ahora solo hay uno)
	if (strcmp("Perseguir", message) == 0)
	{
		//elijo la cola correspondiente para hacer el broadcast
		switch (pk)
		{
		case MessengerAgent::kNormalPursuit:
		{
			for (int i = 0; i < normal_pursuit_.size(); i++)
			{
				if (id_agent != normal_pursuit_[i])
				{
					answer = gm_->agent_.at(normal_pursuit_[i])->SM_Agent(id_agent_to_pursuit);
					if (answer) return true;
				}
			}
			break;
		}
		case MessengerAgent::kHugePursuit:
			for (int i = 0; i < huge_pursuit_.size(); i++)
			{
				if (id_agent != huge_pursuit_[i])
				{
					answer = gm_->agent_.at(huge_pursuit_[i])->SM_Agent(id_agent_to_pursuit);
					if (answer) return true;
				}
			}
			break;
		default:
			printf("Broadcast Wrong\n");
			break;
		}
	}
	return false;
}
void MessengerAgent::RegisterAgent(int id_agent, PursuitKind pk)
{
	//registro en la cola correspondiente al agente
	switch (pk)
	{
	case MessengerAgent::kNormalPursuit:
		normal_pursuit_.push_back(id_agent);
		break;
	case MessengerAgent::kHugePursuit:
		huge_pursuit_.push_back(id_agent);
		break;
	default:
		printf("Pursuit Kind Wrong\n");
		break;
	}
}