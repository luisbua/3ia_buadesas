#include "astar.h"
#include "ESAT_SDK\draw.h"
#include "ESAT_SDK/window.h"

bool AStar::GeneratePath(Point_2 origin, Point_2 destination, Path &path) {
	
	GameManager* gm_ = GameManager::Instance();
	origin.x = round(origin.x / gm_->scale_);
	origin.y = round(origin.y / gm_->scale_);
	destination.x = round(destination.x / gm_->scale_);
	destination.y = round(destination.y / gm_->scale_);
	
	open_list_.clear();
	close_list_.clear();
	path.path_.clear();


 //1 Create a node containing the goal state : node_goal
	node_goal_.position_ = destination;
	FillNode(&node_goal_);
	//2 Create a node containing the start state : node_start
	node_start_.position_ = origin;
	FillNode(&node_start_);
	//3 Put node_start on the OPEN list
	open_list_.push_back(node_start_);
	//4 while the OPEN list is not empty
	while (!open_list_.empty())
	{	
		
		//6 Get the node off the OPEN list with the lowest f and call it node_current
		int tmp = FindLowNodeCost();
		node_current_ = open_list_[tmp];
		open_list_.erase(open_list_.begin() + tmp);
		//7 If node_current is the same state as node_goal : break from the while loop
		if (ComparePoint(node_current_.position_, node_goal_.position_)) 
		{
			node_goal_ = node_current_;
			break;
		}
		//8 Generate each state node_successor that can come after node_current
		//I create 8 node successor and store in a vector (node_successor)
		node_successor_.clear();
		int x = node_current_.position_.x;
		int y = node_current_.position_.y;
		int width = cost_map_->width_;
		int height = cost_map_->height_;
		
		//I check that node_successor do not leave the map 
		if (x >= 1 && y >= 1 && x <= width - 2 && y <= height - 2)
		//Find 8 node_ssuccessor around node_current
		for (int i = (x - 1); i <= (x + 1); i++)
		{
			for (int j = (y - 1); j <= (y + 1); j++)
			{
				// compare node_successor_ with cell cost_map_
				int pos = i + (j*cost_map_->width_);
				//check if is walkable
				if(cost_map_->cost_map_.at(pos).is_walkable_)	
				{
					if (i != x || j != y)	//check if is node_current_
					{
						node_swap_.position_.x = i;
						node_swap_.position_.y = j;
						node_swap_.G_ = MoveCost(node_current_, node_swap_);
						node_swap_.H_ = CalculateHeuristic(node_swap_);
						node_swap_.F_ = node_swap_.G_ + node_swap_.H_;
						node_swap_.father_.x = x;
						node_swap_.father_.y = y;
						node_successor_.push_back(node_swap_);
					}
					}
			}
		}

		/////////////////////////////////////////////																						 
		////////////////Draw Node Succesor///////////
		/////////////////////////////////////////////
		//Descomentar este bloque para visualizar la expansion de la 
		//busqueda del camino
		/*
			ESAT::DrawBegin();
			//DrawSprite(map, 0, 0);
			for (int i = 0; i < node_successor_.size(); i++)
			{
				DrawSprite(gm_->s_open_, node_successor_[i].position_.x * 8, node_successor_[i].position_.y * 8);
			
			}
			for (int i = 0; i < open_list_.size(); i++)
			{
				DrawSprite(gm_->s_close_, open_list_[i].position_.x * 8, open_list_[i].position_.y * 8);
			}
	 		ESAT::DrawEnd();
			ESAT::WindowFrame();
		*/
		
		////////////////////////////////////////////
		////////////////////////////////////////////
		////////////////////////////////////////////

		//9 For each node_successor of node_current
		int j_open = 0;
		int k_close = 0;
			
		for (int i = 0;i < node_successor_.size();i++)
		{
			bool listopen = false;
			bool listclose = false;
			//11 Set the cost of node_successor to be the cost of node_current plus the cost to get to node_successor from node_current
			node_successor_.at(i).G_ = node_current_.G_ + MoveCost(node_current_,node_successor_.at(i));
				
			//12 Find node_successor on the OPEN list
			//13 If node_successor is on the OPEN list but the existing one is as good or better then discard this successor and continue with next successor
			for (int j = 0; j < open_list_.size(); j++)
			{
				if (ComparePoint(node_successor_.at(i).position_, open_list_.at(j).position_))
				{
					listopen = true;
					j_open = j;
					break;
				}						 				
			}

			if (listopen && node_successor_.at(i).G_ >= open_list_.at(j_open).G_) {
				 continue;
			}
			//15 Remove occurences of node_successor from OPEN 
			else { if (listopen) open_list_.erase(open_list_.begin() + j_open);	}
			
			//14 If node_successor is on the CLOSED list but the existing one is as good or better then discard this successor and continue with next successor
			
			for (int k = 0; k < close_list_.size(); k++)
			{
				if (ComparePoint(node_successor_.at(i).position_, close_list_.at(k).position_))
				{
					listclose = true;
					k_close = k;
					break;
				}
			}
			if (listclose && node_successor_.at(i).G_ >= close_list_.at(k_close).G_) {
				continue;
			}
			//15 Remove occurences of node_successor from CLOSED
			else { if (listclose) close_list_.erase(close_list_.begin() + k_close); }
			
			//16 Set the parent of node_successor to node_current
			node_successor_.at(i).father_.x = node_current_.position_.x;
			node_successor_.at(i).father_.y = node_current_.position_.y;
			
			//17 Set h to be the estimated distance to node_goal(using the heuristic function)
			node_successor_.at(i).H_ = CalculateHeuristic(node_successor_.at(i));
			node_successor_.at(i).F_ = node_successor_.at(i).G_ + node_successor_.at(i).H_;
			
			//18 Add node_successor to the OPEN list
			open_list_.push_back(node_successor_.at(i)); 
					
		}
		
		//20 Add node_current to the CLOSED list
		close_list_.push_back(node_current_);
		//printf("\n%d %d", open_list_.size(), close_list_.size());
		
	}
	
	//To check path not found
	if (!open_list_.empty()) {
		CalculatePath(&path);
		return true;
	}
	else {
		printf("\nCamino imposible de hallar");
		return false;
	}
}

bool AStar::PreProcess(CostMap * cost_map)
{
	cost_map_ = cost_map;
	return true;
}

void AStar::ResetData()
{
	open_list_.clear();
	close_list_.clear();
	node_successor_.clear();
}

void AStar::CalculatePath(Path *path_agent)
{
	GameManager* gm_ = GameManager::Instance();
	Point_2 tmp;
	path_agent->current_point_ = -1;
	node_swap_ = node_goal_;
	path_agent->Add(node_goal_.position_);
	while (!ComparePoint(path_agent->path_.front(), node_start_.position_)) {

		tmp = node_swap_.father_;
		path_agent->Add(tmp);
		
		for (int i = 0; i < close_list_.size(); i++)
		{
			if (ComparePoint(close_list_[i].position_, tmp)) {
				node_swap_ = close_list_[i];
				break;
			 }
		}
	}
	path_agent->Add(node_start_.position_);

	//transformo los puntos de mapa de costes a mapa real
	for (int i= 0; i < path_agent->path_.size(); i++)
	{
		path_agent->path_.at(i).x *= gm_->scale_;
		path_agent->path_.at(i).y *= gm_->scale_;
	}
	
}

void AStar::FillNode(Node* node)
{
	float x = node->position_.x;
	float y = node->position_.y;
	float pos = x + (y*cost_map_->width_);
	
	node->G_ = MoveCost(*node,*node);
	node->H_ = CalculateHeuristic(*node);
	node->F_ = node->G_ + node->H_;

}

bool AStar::ComparePoint(Point_2 a, Point_2 b)
{
	if (a.x == b.x && a.y == b.y) return true;
	else return false;
}

int AStar::FindLowNodeCost()
{
	int pos = 0;
	float cost = open_list_[0].F_;
	for (int k = 0; k < open_list_.size(); k++)
	{
		if (open_list_[k].F_ < cost)
		{
			cost = open_list_[k].F_;
			pos = k;
		}
	}
 	return pos;
}

float AStar::CalculateHeuristic(Node node)	//Manhatan
{
	float x, y;
	x = (node_goal_.position_.x - node.position_.x);
	y = (node_goal_.position_.y - node.position_.y);

 //select 1 to Manhatan method or 0 to Module method
#if 1
	//Manhatan
	if (x < 0) x *= -1;
	if (y < 0) y *= -1;

	return (x + y) * 10;

#else
	//Modulo
	Vec_2 v;
	v.x = x;
	v.y = y;
	float dist;
	dist = sqrt((v.x * v.x) + (v.y * v.y));
	return dist * 10;

#endif 
}

float AStar::MoveCost(Node current, Node successor)
{
	if (current.position_.x != successor.position_.x &&
			current.position_.y != successor.position_.y)
	{
			return 1.4f;
	}
	else 
	{ 
			return 1.0f; 
	}
}

