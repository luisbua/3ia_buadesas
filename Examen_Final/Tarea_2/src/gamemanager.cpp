#include "gamemanager.h"


GameManager::GameManager() {

}
GameManager::~GameManager() {

}
void GameManager::Init() {
	/*
	*/
		
	s_soldier_ = ESAT::SpriteFromFile("../../data/Soldier.png");
	s_hostage_ = ESAT::SpriteFromFile("../../data/Hostage.png");
	s_guard_ = ESAT::SpriteFromFile("../../data/Guard.png");
	s_arrow_ = ESAT::SpriteFromFile("../../data/arrow.png");
	s_open_ = ESAT::SpriteFromFile("../../data/open.png");
	s_close_ = ESAT::SpriteFromFile("../../data/close.png");
	s_alarm_ = ESAT::SpriteFromFile("../../data/alarm.png");
	
	map.Load("../../data/map4.bmp");
	cost_map.Load("../../data/map5.bmp");
	
	//Cierro las puertas
	cost_map.UseDoor('A', "Close");
	cost_map.UseDoor('B', "Close");
	
	scale_ = map.width / cost_map.width_;
	cost_map.scale_ = scale_;
	
	soldier_vector_position_ = 0;
	hostage_vector_position_ = 0;
	guard_vector_position_ = 0;
	real_time_to_update_ = 0;

	dist_to_use_door_ = 25.0f;
	
	//going to work yard
	Point_2 p = { 550,280 };
	Point_2 p1 = {615,390};
	Point_2 p2 = { 560,470 };
	go_to_work_.push_back(p2);
	go_to_work_.push_back(p1);
	go_to_work_.push_back(p);
	//working path
	Point_2 w1 = { 430,280 };
	Point_2 w2 = { 640,300 };
	working_.push_back(w2);
	working_.push_back(w1);
	//going to rest zone 
	Point_2 p3 = { 666,510 };
	go_to_rest_.push_back(p1);
	go_to_rest_.push_back(p2);
	go_to_rest_.push_back(p3);

	//yard doors	
	yard_door_right_ = { 675,65 };
	yard_door_left_ = { 325,150 };

	//scape points
	scape_left_ = { 580,25};
	scape_right_ = { 935,380 };
 
	//guard start
	guards_[0].guard_start_ = { 730,100 };
	guards_[0].start_room = 1;
	guards_[1].guard_start_ = { 215,85 };
	guards_[1].start_room = 0;
	guards_[2].guard_start_ = { 375,445 };
	guards_[2].start_room = 11;
	guards_[3].guard_start_ = { 305,235 };
	guards_[3].start_room = 2;
	guards_[4].guard_start_ = { 810,315 };
	guards_[4].start_room = 14;
	guards_[5].guard_start_ = { 810,220 };
	guards_[5].start_room = 8;
	guards_[6].guard_start_ = { 445,115 };
	guards_[6].start_room = 4;
	guards_[7].guard_start_ = { 610,170 };
	guards_[7].start_room = 5;
	guards_[8].guard_start_ = { 810,220 };
	guards_[8].start_room = 8;
	guards_[9].guard_start_ = { 160,255 };
	guards_[9].start_room = 6;
	
	//Interesting Points
	door_A_ = { 820,270 };
	door_B_ = { 155,220 };

	ext_door_A_ ={825,275};
	int_door_A_ ={810,275};
	int_door_B_ ={155,230};
	ext_door_B_ ={155,210};
	
	base_A_ = { 900, 670 };
	base_B_ = { 48,670 };

	//scape for A
	Point_2 s1 = { 805,275 };
	Point_2 s2 = { 940,265 };
	scape_for_A_.push_back(s1);
	scape_for_A_.push_back(s2);
	scape_for_A_.push_back(base_A_);

	
	window_closed = false;
	alarm_ = false;
	alarm_off_ = 1000 * 10;//10 seg

	//Init Rooms data
	FillRooms();
	 
}
void GameManager::FillRooms() {
	Point_2 door_1, door_2, door_3, door_4;

	//el Upper_square y lower_sqare delimitan la zona de 
	//patrulla de cada habitacion

	door_1 = { 250,85 };
	room_[0].id_ = 0;
	room_[0].upper_square_ = { 205,45 };
	room_[0].lower_square_ = { 240,120 };
	room_[0].doors_.push_back(door_1);

	door_1 = { 675,60 };
	door_2 = { 750,155 };
	room_[1].id_ = 1;
	room_[1].upper_square_ = { 690,45 };
	room_[1].lower_square_ = { 780,145 };
	room_[1].doors_.push_back(door_1);
	room_[1].doors_.push_back(door_2);

	door_1 = { 340,265 };
	room_[2].id_ = 2;
	room_[2].upper_square_ = { 285,185 };
	room_[2].lower_square_ = { 330,280 };
	room_[2].doors_.push_back(door_1);
	
	door_1 = { 365,210 };
	room_[3].id_ = 3;
	room_[3].upper_square_ = { 350,125 };
	room_[3].lower_square_ = { 385,200 };
	room_[3].doors_.push_back(door_1);
	
	door_1 = { 465,145 };
	door_2 = { 605,145 };
	room_[4].id_ = 4;
	room_[4].upper_square_ = { 405,90 };
	room_[4].lower_square_ = { 665,135 };
	room_[4].doors_.push_back(door_1);
	room_[4].doors_.push_back(door_2);

	door_1 = { 465,145 };
	door_2 = { 605,145 };
	door_3 = { 435,195 };
	door_4 = { 630,195 };
	room_[5].id_ = 5;
	room_[5].upper_square_ = { 405,155 };
	room_[5].lower_square_ = { 665,175 };
	room_[5].doors_.push_back(door_1);
	room_[5].doors_.push_back(door_2);
	room_[5].doors_.push_back(door_3);
	room_[5].doors_.push_back(door_4);

	door_1 = { 185,255 };
	room_[6].id_ = 6;
	room_[6].upper_square_ = { 130,230 };
	room_[6].lower_square_ = { 175,240 };
	room_[6].doors_.push_back(door_1);

	door_1 = { 750,155 };
	door_2 = { 720,180 };
	door_3 = { 790,210 };
	room_[7].id_ = 7;
	room_[7].upper_square_ = { 730,165 };
	room_[7].lower_square_ = { 775,225 };
	room_[7].doors_.push_back(door_1);
	room_[7].doors_.push_back(door_2);
	room_[7].doors_.push_back(door_3);

	door_1 = { 790,210 };
	room_[8].id_ = 8;
	room_[8].upper_square_ = { 795,200 };
	room_[8].lower_square_ = { 825,240 };
	room_[8].doors_.push_back(door_1);

	door_1 = { 340,320 };
	door_2 = { 345,370 };
	room_[9].id_ = 9;
	room_[9].upper_square_ = { 300,300 };
	room_[9].lower_square_ = { 330,360 };
	room_[9].doors_.push_back(door_1);
	room_[9].doors_.push_back(door_2);

	door_1 = { 330,360 };
	door_2 = { 375,420 };
	room_[10].id_ = 10;
	room_[10].upper_square_ = { 325,380 };
	room_[10].lower_square_ = { 375,420 };
	room_[10].doors_.push_back(door_1);
	room_[10].doors_.push_back(door_2);

	door_1 = { 375,420 };
	door_2 = { 410,450 };
	room_[11].id_ = 11;
	room_[11].upper_square_ = { 360,430 };
	room_[11].lower_square_ = { 400,465 };
	room_[11].doors_.push_back(door_1);
	room_[11].doors_.push_back(door_2);

	door_1 = { 410,450 };
	door_2 = { 460,400 };
	door_3 = { 615,400 };
	room_[12].id_ = 12;
	room_[12].upper_square_ = { 420,405 };
	room_[12].lower_square_ = { 665,460 };
	room_[12].doors_.push_back(door_1);
	room_[12].doors_.push_back(door_2);
	room_[12].doors_.push_back(door_3);

	door_1 = { 760,305 };
	door_2 = { 790,350 };
	room_[13].id_ = 13;
	room_[13].upper_square_ = { 730,315 };
	room_[13].lower_square_ = { 775,455 };
	room_[13].doors_.push_back(door_1);
	room_[13].doors_.push_back(door_2);

	door_1 = { 790,350 };
	room_[14].id_ = 14;
	room_[14].upper_square_ = { 800,300 };
	room_[14].lower_square_ = { 825,370 };
	room_[14].doors_.push_back(door_1);
	
	door_1 = { 570,470 };
	room_[15].id_ = 15;
	room_[15].upper_square_ = { 540,480 };
	room_[15].lower_square_ = { 775,545 };
	room_[15].doors_.push_back(door_1);

	door_1 = { 250,85 };
	door_2 = { 675,60 };
	room_[16].id_ = 16;
	room_[16].upper_square_ = { 260,55 };
	room_[16].lower_square_ = { 665,75 };
	room_[16].doors_.push_back(door_1);
	room_[16].doors_.push_back(door_2);

	door_1 = { 260,170 };
	door_2 = { 225,255 };
	door_3 = { 275,430 };
	room_[17].id_ = 17;
	room_[17].upper_square_ = { 240,170 };
	room_[17].lower_square_ = { 265,465 };
	room_[17].doors_.push_back(door_1);
	room_[17].doors_.push_back(door_2);
	room_[17].doors_.push_back(door_3);

	door_1 = { 310,475 };
	room_[18].id_ = 18;
	room_[18].upper_square_ = { 295,485 };
	room_[18].lower_square_ = { 520,520 };
	room_[18].doors_.push_back(door_1);

	door_1 = { 725,275 };
	room_[19].id_ = 19;
	room_[19].upper_square_ = { 800,260 };
	room_[19].lower_square_ = { 810,275 };
	room_[19].doors_.push_back(door_1);

	door_1 = { 575,660 };
	door_2 = base_B_;
	room_[20].id_ = 20;
	room_[20].upper_square_ = { 80,660 };
	room_[20].lower_square_ = { 540,670 };
	room_[20].doors_.push_back(door_1);
	room_[20].doors_.push_back(door_2);

	door_1 = { 575,580 };
	door_2 = { 200,580 };
	room_[21].id_ = 21;
	room_[21].upper_square_ = { 290,560 };
	room_[21].lower_square_ = { 520,600 };
	room_[21].doors_.push_back(door_1);
	room_[21].doors_.push_back(door_2);

	door_1 = { 200,580 };
	door_2 = { 50,220 };
	room_[22].id_ = 22;
	room_[22].upper_square_ = { 5,300 };
	room_[22].lower_square_ = { 185,590 };
	room_[22].doors_.push_back(door_1);
	room_[22].doors_.push_back(door_2);

	door_1 = { 50,220 };
	door_2 = { 170,20 };
	door_3 = door_B_;
	room_[23].id_ = 23;
	room_[23].upper_square_ = { 5,5 };
	room_[23].lower_square_ = { 170,200 };
	room_[23].doors_.push_back(door_1);
	room_[23].doors_.push_back(door_2);
	room_[23].doors_.push_back(door_3);//Door B

	door_1 = { 170,20 };
	door_2 = { 875,15 };
	room_[24].id_ = 24;
	room_[24].upper_square_ = { 5,170 };
	room_[24].lower_square_ = { 875,25 };
	room_[24].doors_.push_back(door_1);
	room_[24].doors_.push_back(door_2);

	door_1 = { 875,15 };
	door_2 = { 905,85 };
	room_[25].id_ = 25;
	room_[25].upper_square_ = { 880,5 };
	room_[25].lower_square_ = { 910,80 };
	room_[25].doors_.push_back(door_1);
	room_[25].doors_.push_back(door_2);

	door_1 = { 905,85 };
	door_2 = { 870,255 };
	room_[26].id_ = 26;
	room_[26].upper_square_ = { 845,100 };
	room_[26].lower_square_ = { 880,245 };
	room_[26].doors_.push_back(door_1);
	room_[26].doors_.push_back(door_2);

	door_1 = { 945,290 };
	door_2 = base_A_;
	room_[27].id_ = 27;
	room_[27].upper_square_ = { 920,290 };
	room_[27].lower_square_ = { 950,680 };
	room_[27].doors_.push_back(door_1);
	room_[27].doors_.push_back(door_2);
}
bool GameManager::CalcPathToFoe(Point_2 orig, Point_2 dest)
{
	//Funcion que mira si hay obstaculos entre dos agentes
	orig.x = round(orig.x);
	orig.y = round(orig.y);
	dest.x = round(dest.x);
	dest.y = round(dest.y);

	std::vector<Point_2> vec_tmp;
	Vec_2 tmp = vo.CalcDirection(orig,dest);
	Point_2 tmp2 = orig;
	Point_2 tmp3 = orig;

	while (!vo.ComparePoint_2(tmp3,dest))
	{
		tmp2.x = tmp2.x + tmp.x;
		tmp2.y = tmp2.y + tmp.y;

		tmp3.x = round(tmp2.x);
		tmp3.y = round(tmp2.y);

		vec_tmp.push_back(tmp3);
	}

	for (int i = 0; i < vec_tmp.size(); i++)
	{
		if (!cost_map.isWalkable(vec_tmp.at(i), true)) {
				return false;
		}
	}
	return true;
}
GameManager* GameManager::instance_;