#include "map.h"


Map::Map() {}

void Map::Load(const char *path)
{
	map = SpriteFromFile(path);
	width = SpriteWidth(map);
	height = SpriteHeight(map);
}

void Map::Draw()
{
	DrawSprite(map, 0, 0);
}
