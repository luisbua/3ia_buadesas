#include "cost_map.h"


CostMap::CostMap() {}

bool CostMap::Load(const char *path) {

	char extension[4] = { "   " };
	int length = strlen(path);
	 
	for (int  i = 0; i < 3; i++)
	{
		extension[i] = path[length - 3 + i];
	}
	if (strcmp("bmp",extension) == 0) 
	{
		LoadBMP(path);
		return 1;
	}
	else 
	{
		if (strcmp("txt", extension) == 0) 
		{
			LoadTXT(path);
			return 1;
		}
	}

	return 0;
}
bool CostMap::LoadBMP(const char *path)
{
	map_handle_ = SpriteFromFile(path);
	width_ = SpriteWidth(map_handle_);
	height_ = SpriteHeight(map_handle_);
	CalcCellCostBMP();

	return true;
}
void CostMap::CalcCellCostBMP()
{
	unsigned char outRGBA[4];	 
		 
	for (int i = 0; i < height_; i++)
	{
		for (int j = 0; j < width_; j++)
		{
			Cell cell;
			cell.position_.x = j;
			cell.position_.y = i;
			cell.is_walkable_ = true;

			//read pixel color
			SpriteGetPixel(map_handle_, j, i, outRGBA);

			///////Used to First task////////////	
			//Walkable (white)
			if (outRGBA[0] == 255 && outRGBA[1] == 255 && outRGBA[2] == 255)
				cell.cost_ = 10;
			//Wall (black)
			else if (outRGBA[0] == 0 && outRGBA[1] == 0 && outRGBA[2] == 0)
			{
				cell.cost_ = 100;
				cell.is_walkable_ = false;
			}
			
			///////////////////////////////////////////////////
			/////////////Prepared to second task///////////////
			///////////////////////////////////////////////////

			//////////////////Walk surface//////////////////////
			//Door Open (green)
			else if (outRGBA[0] == 0 && outRGBA[1] == 255 && outRGBA[2] == 0)
				cell.cost_ = 15;
			//Base inner (light blue)
			else if (outRGBA[0] == 0 && outRGBA[1] == 155 && outRGBA[2] == 219)
				cell.cost_ = 20;
			//Base outer (dark blue)
			else if (outRGBA[0] == 0 && outRGBA[1] == 111 && outRGBA[2] == 155)
				cell.cost_ = 20;
			//Bridge (light grey)
			else if (outRGBA[0] == 84 && outRGBA[1] == 75 && outRGBA[2] == 64)
				cell.cost_ = 25;
			//Grass	(dark green)
			else if (outRGBA[0] == 88 && outRGBA[1] == 106 && outRGBA[2] == 28)
				cell.cost_ = 30;
			//Rest zone (orange)
			else if (outRGBA[0] == 208 && outRGBA[1] == 115 && outRGBA[2] == 79)
				cell.cost_ = 40;
			//Working Zone (grey)
			else if (outRGBA[0] == 84 && outRGBA[1] == 84 && outRGBA[2] == 84)
				cell.cost_ = 50;
			//Sand (brown)
			else if (outRGBA[0] == 162 && outRGBA[1] == 131 && outRGBA[2] == 86)
				cell.cost_ = 60;

			
			/////////////////No Walk Surface////////////////////
			//River (blue)
			else if (outRGBA[0] == 0 && outRGBA[1] == 162 && outRGBA[2] == 232)
			{
				cell.cost_ = 80;
				cell.is_walkable_ = false;
			}
			//Door Close (red)
			else if (outRGBA[0] == 220 && outRGBA[1] == 3 && outRGBA[2] == 5)
			{
				cell.cost_ = 90;
				cell.is_walkable_ = false;
			}
			else cell.cost_ = 0;

			////////add cell to vector/////////
			cost_map_.push_back(cell);
		}
	}
	//CloseDoor('A', "Close");
	//CloseDoor('B', "Close");
	///////////////////////////////////////////////////
	///////////////////////////////////////////////////
	///////////////////////////////////////////////////
}
bool CostMap::LoadTXT(const char *path)
{
	int x = 0, y = 0;
	Cell cell;
	pFile = fopen(path, "r");
	if (pFile == NULL) perror("Error opening file");
	else
	{
		while (!feof(pFile))
		{
			fread(buffer,sizeof(char), 1,pFile);
			switch (buffer[0])
			{
			case 10:
				
				height_ = y;
				y++;
				x = 0;
				break;
			
			case 32:
				cell.position_.x = x;
				cell.position_.y = y;
				cell.is_walkable_ = true;
				cell.cost_ = 10;
				cost_map_.push_back(cell);
				x++;
				width_ = x;
				break;
			
			case 42:
				cell.position_.x = x;
				cell.position_.y = y;
				cell.is_walkable_ = false;
				cell.cost_ = 100;
				cost_map_.push_back(cell);
				x++;
				width_ = x;
				break;
			
			default:
				break;
			}

		}
		fclose(pFile);
	}
	return true;
}

bool CostMap::isWalkable(Point_2 position, bool callexternal)
{
	
	if (callexternal) {
		position.x = round(position.x / scale_);
		position.y = round(position.y / scale_);
		}
	return cost_map_[(position.y * width_) + position.x].is_walkable_;

}
void CostMap::Print()
{
	printf("\n");
	int takecost;

	for (int i = 0; i < height_; i++)
	{
		for (int j = 0; j < width_; j++)
		{
			takecost = cost_map_[(i*width_) + j].cost_;

			switch (takecost) {
			case 10: printf("%c",219); break; //White
			case 15: printf("%c", 241); break; //Door open
			case 20: printf("%c",248); break;	//Base
			case 25: printf("%c",227); break;	//Bridge
			case 30: printf("%c",177); break;	//Grass
			case 40: printf("%c",178); break;	//Rest Zone
			case 50: printf("%c",38); break;	//Working Zone
			case 60: printf("%c",176); break;	//Sand
			case 80: printf("%c",186); break;	//River
			case 90: printf("%c",246); break;	//Door close
			case 100: printf("%c",255); break; //Black
			default: printf("%c", 255); break; //Black
		 	};
		}
		printf("\n");
	}
}
void CostMap::Draw(int x, int y)
{
	DrawSprite(map_handle_, x, y);
}
void CostMap::UseDoor(char door, char* state) {

	switch (door)
	{
	case 'A':
		if (strcmp(state, "Open") == 0) {
			for (int  i = 0; i < 4; i++)
			{
				cost_map_[(door_A_costmap_[i].y * width_) + 
					door_A_costmap_[i].x].is_walkable_ = true;
			}
			door_A_open_ = true;
		}
		else {
			for (int i = 0; i < 4; i++)
			{
				cost_map_[(door_A_costmap_[i].y * width_) +
					door_A_costmap_[i].x].is_walkable_ = false;
			}
			door_A_open_ = false;
		}
		break;
	case 'B':
		if (strcmp(state, "Open") == 0) {
			for (int i = 0; i < 3; i++)
			{
				cost_map_[(door_B_costmap_[i].y * width_) +
					door_B_costmap_[i].x].is_walkable_ = true;
			}
			door_B_open_ = true;
		}
		else {
			for (int i = 0; i < 3; i++)
			{
				cost_map_[(door_B_costmap_[i].y * width_) +
					door_B_costmap_[i].x].is_walkable_ = false;
			}
			door_B_open_ = false;
		}
		break;
	default:
		printf("\nLa puerta no es correcta");
		break;
	}
}