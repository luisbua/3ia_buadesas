#include "agent.h"

Agent::Agent(){}
Agent::~Agent(){}

void Agent::Init(int id, AgentType ag, float x, float y, int room)
{
	id_ = id;
	agent_type_ = ag;
	dist_ = 1000;
	nearest_id_ = id;
	chase_dinstance_ = 80;
	flee_distance_ = 50;
	rest_distance_ = 150;
	tracking_at_ = id;
	tracking_me_ = id;
	speed_ = 0.02f; //0.02 original
		
	resting_time_ = 0;
	time_working_ = 0;
	
	start_.x = x;
	start_.y = y;
	random_start_ = start_;
	pos_ = start_;
	 
	ResetDirection();
		
	reached_ = true;
	patron_choose_ = false;
	time_reached_ = true;
	length_unit_patron_ = 30;
	current_unit_patron_ = length_unit_patron_;
	nav_point_reached_ = 0;
	astar_calculated = false;
	go_to_work_ = false;
	working_ = false;
	go_to_rest_ = false;
	go_A_B_ = false;
	go_B_A_ = false;
	inside_ = true;
	patrolling_ = true;
	closing_door_ = false;
	fleeing_ = false;
	scape_to_A_ = false;
	awaiting_ = false;
	chasign_soldier_ = false;

	switch (ag)
	{
	case Agent::kSoldier:
		state_type_ = kSearchDoor;
		speed_ *= 3;
		inside_ = false;
		SearchDoor();
		break;
	case Agent::kGuard:
		state_type_ = kPatroll;
		in_room_ = room;
		adyacent_tmp_ = room;
		break;
	case Agent::kHostage:
		if (id_ < 5) {
		 	state_type_ = kResting;
			in_room_ = 15;
		}
		else {
			state_type_ = kWorking;
		}
		resting_time_ = ESAT::Time();
		break;
	default:
		break;
	}
}
/////////////UPDATE///////////////
void Agent::Update(int step)											  
{
	Mind(step);
	Body(step);
}
void Agent::Mind(int step)
{
	switch (state_type_)
	{
	case Agent::kWorking: Working(); break;
	case Agent::kWalkingTo: Working(); break;
	case Agent::kGoToWorking: GoToWorking(); break;
	case Agent::kGoToRest: GoToRest(); break;
	case Agent::kChasing: Chasing(); break;
	case Agent::kFleeing: Fleeing(); break;
	case Agent::kResting: Resting(); break;
	case Agent::kAwaitng: Awaiting(); break;
	case Agent::kPatroll: Patrolling(); break;
	case Agent::kUseDoor: UseDoor(); break;
	case Agent::kSearchDoor: SearchDoor(); break;
	case Agent::kScape: Scape(); break;
	default: printf("State Type Wrong\n");	break;
	}
}
void Agent::Body(int step)
{
	switch (move_type_)
	{
	case Agent::kDeterminist: DeterministMovment(step); break;
	case Agent::kRandom: RandomMovment(step);	break;
	case Agent::kTrack: TrackMovment(step);	break;
	case Agent::kPatron: PatronMovment(step); break;
	case Agent::kAwaitingMove: AwaitingMovment(step); break;
	default: printf("Move Type Wrong\n"); break;
	}
}
/////////////DRAW/////////////////
void Agent::Draw()
{
	switch (agent_type_)
	{
	case Agent::kSoldier:
		ESAT::DrawSprite(gm_->s_soldier_, pos_.x, pos_.y);
		break;
	case Agent::kGuard:
		ESAT::DrawSprite(gm_->s_guard_, pos_.x, pos_.y);
		break;
	case Agent::kHostage:
		ESAT::DrawSprite(gm_->s_hostage_, pos_.x, pos_.y);
		break;
	default:
		printf("Draw Wrong\n");
	}
}
///////////////MIND///////////////
void Agent::Working()
{
	if (!working_) {
		nav_points_.path_.clear();
		for (int i = 0; i < gm_->working_.size(); i++)
		{
			nav_points_.path_.push_back(gm_->working_.at(i));
		}
		nav_points_.current_point_ = -1;
		move_type_ = kDeterminist;
		working_ = true;
		time_working_ = rand() % 25000 + 10000; //work for 3-10 seg
		resting_time_ = ESAT::Time();
		reached_ = true;
		nav_points_.end_path_ = false;
		speed_ /= 3;
	 }
	if (nav_points_.end_path_) nav_points_.current_point_ = -1;

	if (ESAT::Time() - resting_time_ > time_working_)
	{
		state_type_ = kGoToRest;
		go_to_rest_ = false;
		nav_points_.current_point_ = -1;
		speed_ *= 3;

		
	}

	//compruebo si hay alerta
	if (gm_->alarm_) {
		//compruebo si hay puertas abiertas
		if (gm_->cost_map.door_A_open_ || gm_->cost_map.door_B_open_) {
			state_type_ = kScape;
			}
		}
}
void Agent::WalkingTo() {

}
void Agent::GoToWorking()
{
	if (!go_to_work_) {
		nav_points_.path_.clear();
		for (int i = 0; i < gm_->go_to_work_.size(); i++)
		{
			nav_points_.path_.push_back(gm_->go_to_work_.at(i));
		}
		move_type_ = kDeterminist;
		nav_points_.current_point_ = -1;
		go_to_work_ = true;
		reached_ = true;
		nav_points_.end_path_ = false;
		
	}
	if (nav_points_.end_path_) {
		state_type_ = kWorking;
		working_ = false;
	}
}
void Agent::GoToRest() {
	if (!go_to_rest_) {
		nav_points_.path_.clear();
		for (int i = 0; i < gm_->go_to_rest_.size(); i++)
		{
			nav_points_.path_.push_back(gm_->go_to_rest_.at(i));
		}
		//nav_point_reached_ = 0;
		move_type_ = kDeterminist;
		nav_points_.current_point_ = -1;
		go_to_rest_ = true;
		reached_ = true;
		nav_points_.end_path_ = false;

	}
	if (nav_points_.end_path_) {
		state_type_ = kResting;
		resting_time_ = ESAT::Time();
		reached_ = true;
		
	}
}
void Agent::Chasing()
{
	
	//selecciono el tipo de movimiento para Chasing
	move_type_ = kTrack;
	//////Persiguiendo Soldados/////
	if (chasign_soldier_) {
	//claculo distancia con el objetivo que persigo
		dist_ = vo.CalcDistance(pos_, gm_->soldier_.at(tracking_at_)->pos_);
		if (dist_ > 75.5f)
		{
			state_type_ = kPatroll;
			patrolling_ = true;
			gm_->soldier_.at(tracking_at_)->tracking_me_ = tracking_at_;
			//resting_time_ = ESAT::Time();

		}
	}
	///////Persiguiendo Rehenes///////
	else {
		dist_ = vo.CalcDistance(pos_, gm_->hostage_.at(tracking_at_)->pos_);
		if (!gm_->alarm_)
		{
			state_type_ = kPatroll;
			patrolling_ = true;
			gm_->hostage_.at(tracking_at_)->tracking_me_ = tracking_at_;
			//resting_time_ = ESAT::Time();
		}
	}
	
}
void Agent::Fleeing()
{		
	if (fleeing_) {
		//si estoy fuera
		if (!inside_) {
			if (pos_.x < 480) {
				nav_points_.path_.clear();
				if (astar_.GeneratePath(pos_, gm_->scape_left_, nav_points_)) {
					move_type_ = kDeterminist;
					nav_points_.current_point_ = -1;
					reached_ = true;
					nav_points_.end_path_ = false;
					fleeing_ = false;
				}
			}
			else {
				nav_points_.path_.clear();
				if (astar_.GeneratePath(pos_, gm_->scape_right_, nav_points_)) {
					move_type_ = kDeterminist;
					nav_points_.current_point_ = -1;
					reached_ = true;
					nav_points_.end_path_ = false;
					fleeing_ = false;
				}
			}
		}	
		//si estoy dentro
		else {
			nav_points_.path_.clear();
			if (astar_.GeneratePath(pos_, gm_->int_door_A_, nav_points_)) {
				move_type_ = kDeterminist;
				nav_points_.current_point_ = -1;
				reached_ = true;
				nav_points_.end_path_ = false;
				fleeing_ = false;
			}
		}
	}
	//chequeo si he llegado al final del camino o si no me persiguen
	else {
		//compruebo si estoy cerca de un guardia
		for (int i = 0; i < gm_->guard_.size(); i++)
		{
			if (vo.CalcDistance(pos_, gm_->guard_.at(i)->pos_) < 50.0f) {
				//miro que no hayan paredes de por medio
				if (gm_->CalcPathToFoe(pos_, gm_->guard_.at(i)->pos_)) {

					state_type_ = kChasing;
					tracking_me_ = i;
					
				}
			}
			else {
				tracking_me_ = id_;
				tracking_at_ = id_;
				nav_points_.path_.clear();
				if (astar_.GeneratePath(pos_, gm_->ext_door_A_, nav_points_)) {
					move_type_ = kDeterminist;
					nav_points_.current_point_ = -1;
					reached_ = true;
					nav_points_.end_path_ = false;

				}
			}
		}
		
		if (tracking_me_ == id_) {
			state_type_ = kSearchDoor;
			astar_calculated = false;
			//move_type_ = kAwaitingMove;
		}
		//si no persiguen pero he llegado a la puerta
		else if (nav_points_.end_path_) {
			if (!scape_to_A_) {
				 if (!gm_->cost_map.door_A_open_) {
					OpenDoor();
				}
				nav_points_.path_.clear();
				if (astar_.GeneratePath(pos_, gm_->scape_right_, nav_points_)) {
					move_type_ = kDeterminist;
					nav_points_.current_point_ = -1;
					reached_ = true;
					nav_points_.end_path_ = false;
				}
			}
			else {
				state_type_ = kAwaitng;
			}
		}

	}
}
void Agent::Resting()
{
	switch (agent_type_)
	{
	case Agent::kSoldier:
		break;
	case Agent::kGuard:
		move_type_ = kAwaitingMove;
		break;
	case Agent::kHostage:
		move_type_ = kRandom;
		in_room_ = 15;
		break;
	default:
		break;
	}
	

	if (gm_->alarm_ || ESAT::Time() - resting_time_ > 6000)//rest for 6 seg
	{
		state_type_ = kGoToWorking;
		go_to_work_ = false;
	}
}
void Agent::Awaiting() {
	
	switch (agent_type_)
	{
	case Agent::kSoldier:
		
		break;
	case Agent::kGuard:
		break;
	case Agent::kHostage:
		move_type_ = kAwaitingMove;
		
		break;
	default:
		printf("\nAgente Incorrecto");
		break;
	}
	
}
void Agent::Patrolling() {
	

	//si estoy en la habitacion 19 y la puerta esta abierta
	if (in_room_ == 19 && gm_->cost_map.door_A_open_ ) {
		if (!closing_door_) {
			gm_->alarm_ = true;
			gm_->alarm_time_ = ESAT::Time();
			//printf("\nAlarm in A");	
			move_type_ = kDeterminist;
			nav_points_.path_.clear();
			nav_points_.current_point_ = -1;
			reached_ = true;
			nav_points_.Add(gm_->int_door_A_);
			patrolling_ = false;
			closing_door_ = true;
		}
		
	}
	else if (in_room_ == 6 && gm_->cost_map.door_B_open_) {
		if (!closing_door_) {
			gm_->alarm_ = true;
			gm_->alarm_time_ = ESAT::Time();
			//printf("\nAlarm in B");
			move_type_ = kDeterminist;
			nav_points_.path_.clear();
			nav_points_.current_point_ = -1;
			reached_ = true;
			nav_points_.Add(gm_->int_door_B_);
			patrolling_ = false;
			closing_door_ = true;
		}
	}
	
	if (patrolling_) {
		move_type_ = kRandom;

		int time = rand() % 10000 + 5000;
		if (ESAT::Time() - patrolling_time_ > time)//patroll between 5 -15 seg
		{
			 adyacent_tmp_ = SelecAdyacent(in_room_);
			GoToNextRoom(adyacent_tmp_);
			
			move_type_ = kDeterminist;
			nav_points_.current_point_ = -1;
			reached_ = true;
			nav_points_.end_path_ = false;
			patrolling_ = false;
		}
	}
	else {
		if (nav_points_.end_path_) {
			patrolling_ = true;
			patrolling_time_ = ESAT::Time();
			in_room_ = adyacent_tmp_;
			if (closing_door_) {
				state_type_ = kUseDoor;
				closing_door_ = false;
			}
			
		}
	}
 	//compruebo si estoy cerca de un soldado
		for (int i = 0; i < gm_->soldier_.size(); i++)
		{
			if (vo.CalcDistance(pos_, gm_->soldier_.at(i)->pos_) < 50.0f) {
				//miro que no hayan paredes de por medio
				if (gm_->CalcPathToFoe(pos_, gm_->soldier_.at(i)->pos_)) {

					state_type_ = kChasing;
					tracking_at_ = i;
					gm_->soldier_.at(i)->tracking_me_ = id_;
					chasign_soldier_ = true;
				}
			}
		}
		//si estoy en alarma tambien persigo prisioneros
		if (gm_->alarm_) {
			for (int i = 0; i < gm_->hostage_.size(); i++)
			{
				if (vo.CalcDistance(pos_, gm_->hostage_.at(i)->pos_) < 50.0f) {
					//miro que no hayan paredes de por medio
					if (gm_->CalcPathToFoe(pos_, gm_->hostage_.at(i)->pos_)) {

						state_type_ = kChasing;
						tracking_at_ = i;
						gm_->hostage_.at(i)->tracking_me_ = id_;
						chasign_soldier_ = false;
					}
				}
			}
		}
	
	//detecto si ha pasado el tiempo de la alarma y la apago
	if (gm_->alarm_ && ESAT::Time() - gm_->alarm_time_ > gm_->alarm_off_) {
		gm_->alarm_ = false;
	}
}
void Agent::SearchDoor(){
	
	inside_ = CheckInside();
	//compruebo si las puertas estan abiertas
	if (gm_->cost_map.door_A_open_ && gm_->cost_map.door_B_open_) {
		state_type_ = kScape;

		awaiting_ = false;
		tracking_at_ = id_;
		tracking_me_ = id_;
	}
	//Chequeo si he de calcular el camino o ya estoy en el//////
	else if (!astar_calculated) {
	//compruebo si las dos puertas estan cerradas
   if (!gm_->cost_map.door_A_open_ && !gm_->cost_map.door_B_open_) {
			//si estan cerradas las dos y estoy fuera abro A
				if (inside_) {
					dest_ = gm_->int_door_A_;
				}
				else {
					dest_ = gm_->ext_door_A_;
				}
		}
		//si hay alguna abierta compruebo si es A  
		else if (!gm_->cost_map.door_A_open_) {
			//si A esta cerrada B esta abierta
			//compruebo si estoy en la puerta B
			if (vo.CalcDistance(pos_, gm_->door_B_) < gm_->dist_to_use_door_) {
				//si estoy en B y esta abierta abro A desde dentro
				dest_ = gm_->yard_door_left_;
				go_B_A_ = true;
				inside_ = true;
			}
			//no estoy cerca de B
			else {
				//estoy dentro
				if (inside_) {
					dest_ = gm_->yard_door_left_;
					printf("\nA Cerrada B Abierta, dentro elijo yard_left %d", id_);
					if (gm_->cost_map.door_B_open_) {
						dest_ = gm_->int_door_A_;
						printf("\nA Cerrada B Abierta, dentro elijo int_A %d", id_);
					}
					else {
						dest_ = gm_->int_door_B_;
						printf("\nA Cerrada B Cerrada, dentro elijo int_B %d", id_);
					}
				}
				//estoy fuera
				else {
					
					if (gm_->cost_map.door_B_open_) {
						if (pos_.x > 170) {
							dest_ = gm_->ext_door_A_;
							printf("\nA Cerrada B Abierta, fuera elijo ext_A %d", id_);
						}
						else {
							dest_ = gm_->yard_door_left_;
							printf("\nA Cerrada B Abierta, fuera elijo yard_left %d", id_);
						}
					}
					else {
						dest_ = gm_->ext_door_A_;
						printf("\nA Cerrada B Cerrada, fuera elijo ext_A %d", id_);
					}
				}
			}
		}
		else {
			//Si esta abierta A es que B esta cerrada
			//compruebo si estoy en la puerta A
			if (vo.CalcDistance(pos_, gm_->door_A_) < gm_->dist_to_use_door_) {
				//si estoy en A y esta abierta abro B desde dentro
				dest_ = gm_->yard_door_right_;
				go_A_B_ = true;
				inside_ = true;
				printf("\nA Abierta B Cerrada, dentro elijo yard_right %d",id_);
			}
			else {
				//si no estoy cerca A abro B 
				if (inside_) {
					dest_ = gm_->int_door_A_;
					printf("\nA Abierta B Cerrada, dentro elijo int_A %d", id_);
				}
				else {
					//si estoy en el exterior y B esta cerrada voy por A
					dest_ = gm_->ext_door_A_;
					printf("\nA Abierta B Cerrada, dentro elijo int_A %d", id_);
				}
			}
		}
		//calculo el path segun el destino escogido
		if (!astar_calculated) {
			astar_.PreProcess(&gm_->cost_map);
			nav_points_.path_.clear();
			if (astar_.GeneratePath(pos_, dest_, nav_points_)) {
				move_type_ = kDeterminist;
				nav_points_.current_point_ = -1;
				reached_ = true;
				nav_points_.end_path_ = false;
				astar_calculated = true;
			}
		}
	}
	///////////chequeo si he llegado al final del camino///////
	else if (nav_points_.end_path_) {
		if (go_A_B_) {
			dest_ = gm_->int_door_B_;
			astar_.PreProcess(&gm_->cost_map);
			nav_points_.path_.clear();
			if (astar_.GeneratePath(pos_, dest_, nav_points_)) {
				move_type_ = kDeterminist;
				nav_points_.current_point_ = -1;
				reached_ = true;
				nav_points_.end_path_ = false;
				astar_calculated = true;
				go_A_B_ = false;
			}
		}
		else if (go_B_A_) {
				dest_ = gm_->int_door_A_;
				astar_.PreProcess(&gm_->cost_map);
				nav_points_.path_.clear();
				if (astar_.GeneratePath(pos_, dest_, nav_points_)) {
					move_type_ = kDeterminist;
					nav_points_.current_point_ = -1;
					reached_ = true;
					nav_points_.end_path_ = false;
					astar_calculated = true;
					go_B_A_ = false;
				}
			}
			else {
				state_type_ = kUseDoor;
				move_type_ = kAwaitingMove;
				astar_calculated = false;
				printf("\nFin Astar");
			}
		
	}		
	//compruebo si esta la a larma y alguien me persigue
	if (gm_->alarm_ && tracking_me_ != id_) {
		state_type_ = kFleeing;
		fleeing_ = true;
		go_A_B_ = false;
		go_B_A_ = false;
	}
}
void Agent::UseDoor() {
	//move_type_ = kAwaitingMove;
	switch (agent_type_)
	{
		
	case Agent::kSoldier:
		//Compruebo en que puerta estoy, si mi X < 480 estoy en B 
		if (pos_.x < 480) {
			//compruebo si estoy dentro o fuera de la puerta B 
			//si estoy por dentor la puedo abrir
			if (pos_.y > gm_->door_B_.y) {
				OpenDoor();
				state_type_ = kSearchDoor;
				astar_calculated = false;
				//printf("\nEstoy por dentro de B");
				
			}
			//else printf("\nEstoy por fuera de B" );
		}
		//si no estoy en la puerta B estoy en la puerta A
		else {
			//compruebo si estoy dentro o fuera de la puerta A
			if (pos_.x < gm_->door_A_.x) {
				OpenDoor();
				state_type_ = kSearchDoor;
				astar_calculated = false; 
			//	printf("\nEstoy por dentro de A");
			}
			else {
				OpenDoor();
				state_type_ = kSearchDoor;
				astar_calculated = false;
				//printf("\nEstoy por fuera de A");
			}
		}
		break;
	case Agent::kGuard:
		CloseDoor();
		state_type_ = kPatroll;
		break;
	case Agent::kHostage:
		printf("\nLos Rehenes no pueden usar las puertas");
		break;
	default:
		break;
	}
}
void Agent::Scape() {
	////////////////Soldado///////////////////////
	if (agent_type_ == kSoldier) {
		//si las dos puertas estan abiertas
		if (gm_->cost_map.door_A_open_ && gm_->cost_map.door_B_open_) {
			if (!awaiting_) {
				//compruebo si estoy cerca de A
				if (vo.CalcDistance(pos_, gm_->door_A_) < gm_->dist_to_use_door_) {
					nav_points_.path_.clear();
					if (astar_.GeneratePath(pos_, gm_->scape_right_, nav_points_)) {
						move_type_ = kDeterminist;
						nav_points_.current_point_ = -1;
						reached_ = true;
						nav_points_.end_path_ = false;
						awaiting_ = true;
						astar_calculated = false;
					}
				}
				else {
					if (vo.CalcDistance(pos_, gm_->door_B_) < gm_->dist_to_use_door_) {
						nav_points_.path_.clear();
						if (astar_.GeneratePath(pos_, gm_->scape_left_, nav_points_)) {
							move_type_ = kDeterminist;
							nav_points_.current_point_ = -1;
							reached_ = true;
							nav_points_.end_path_ = false;
							awaiting_ = true;
							astar_calculated = false;
						}
					}
				}
			}
		}
		else {
			state_type_ = kSearchDoor;
			astar_calculated = false;
			reached_ = true;
			nav_points_.end_path_ = false;
		}
	}
	else {
		/////////////////Rehen////////////////////
		inside_ = CheckInside();
		if (agent_type_ == kHostage) {
			//compruebo que puerta esta abierta
			if (!scape_to_A_) {
				if (gm_->cost_map.door_A_open_) {
					nav_points_.path_.clear();
					for (int i = 0; i < gm_->scape_for_A_.size(); i++)
					{
						nav_points_.path_.push_back(gm_->scape_for_A_.at(i));
					}
					move_type_ = kDeterminist;
					nav_points_.current_point_ = -1;
					reached_ = true;
					nav_points_.end_path_ = false;
					scape_to_A_ = true;
					speed_ *= 3;
				}
			}
			else {
				//compruebo si la puerta sigue abierta
				if (!gm_->cost_map.door_A_open_) {
					if (pos_.x < 800) {		//estoy por dentro
						state_type_ = kWorking;
						scape_to_A_ = false;
						working_ = false;
					}
					else {

					}
					
				}
				else {
					if (nav_points_.end_path_) {
						state_type_ = kAwaitng;
						nav_points_.path_.clear();
						nav_points_.path_.push_back(gm_->base_A_);
					}
				}
			}
		}
	}
}
///////////////BODY///////////////
void Agent::DeterministMovment(int step)
{
	//Si alcanzo el punto de navegacion selcciono
	//el siguiente punto de navegacion
	if (reached_)
	{
		dest_ = nav_points_.NextPoint();
		direction_ = vo.CalcDirection(pos_, dest_);
		
		reached_ = false;
	}
	else
	//me muevo hacia el punto de navegacion
	{
		pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
		if (vo.CalcDistance(pos_, dest_) <= 5.5f)
		{
			reached_ = true;
		}
	}
}
void Agent::RandomMovment(int step)
{
	GameManager* gm_ = GameManager::Instance();
	//Si alcanzo el punto de navegacion selcciono
	//el siguiente punto de navegacion de forma aleatoria
	//dentro de la zona delimitada
	if (reached_)
	{
		//datos para la habitacion//
		int a = gm_->room_[in_room_].lower_square_.x -
						gm_->room_[in_room_].upper_square_.x;
															 
		int b = gm_->room_[in_room_].lower_square_.y -
						gm_->room_[in_room_].upper_square_.y;
															 
			dest_.x = rand() % a + gm_->room_[in_room_].upper_square_.x;
			dest_.y = rand() % b + gm_->room_[in_room_].upper_square_.y;
					
		//if (id_ == 0) printf("\nX: %3.0f Y: %3.0f",dest_.x, dest_.y);
		direction_ = vo.CalcDirection(pos_, dest_);
		reached_ = false;
	}
	else
	//me muevo hacia el punto de navegacion
	{
		pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
		if (vo.CalcDistance(pos_, dest_) <= 5.5f || 
			!gm_->cost_map.isWalkable(pos_,true))//compruebo que al punto donde voy no es un muro
		{
			reached_ = true;
		}
	}
}
void Agent::TrackMovment(int step)
{
	if (state_type_ == kChasing)
	{
		//calculo la direccion hacia mi objetivo
		//////Persigo Soldado////
		if (chasign_soldier_) {
			//compruebo si hay obstaculos por medio
			if (gm_->CalcPathToFoe(pos_, gm_->soldier_.at(tracking_at_)->pos_)) {
				direction_ =
					vo.CalcDirection(pos_, gm_->soldier_.at(tracking_at_)->pos_);
				pos_ = vo.CalcNewPosition(pos_, direction_, step, speed_);
				
			}
			else {
				state_type_ = kPatroll;
				patrolling_ = true;
				gm_->soldier_.at(tracking_at_)->tracking_me_ = tracking_at_;
			}
			
			}
		///////Persigo Rehen////
		else {
			if (gm_->CalcPathToFoe(pos_, gm_->hostage_.at(tracking_at_)->pos_)) {
				direction_ =
					vo.CalcDirection(pos_, gm_->hostage_.at(tracking_at_)->pos_);
				pos_ = vo.CalcNewPosition(pos_, direction_, step, speed_);
			}
			else {
				state_type_ = kPatroll;
				patrolling_ = true;
				gm_->hostage_.at(tracking_at_)->tracking_me_ = tracking_at_;
			}
		}
	}
}
void Agent::PatronMovment(int step)
{
	/*
	//si han pasado 2 seg leo la nueva instruccion
	if (time_reached_)
	{
		//Dentro de cada instruccion he de moverme durante
		//un tiempo, cada frame incremento current_unit_patron
		//para saber en que parte de la instruccion estoy,
		//si estoy en 0 es que estoy en el inicio de la intruccion
		if (current_unit_patron_ < length_unit_patron_ && 
			  patron_[patron_point_reached_] != kStop)
		{
			current_unit_patron_++;
		}
		else
		{
			current_unit_patron_ = 0;
			//voy recorriendo las instrucciones del patron
			if (patron_point_reached_ < patron_.size() - 1)
				patron_point_reached_++;
			else
			{
				patron_point_reached_ = 0;
				ResetDirection();
			}
		}
		//Ejecuto la accion segun la instruccion recibida 		
		switch (patron_[patron_point_reached_])
		{
			case Agent::kForward:
				pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
				break;
			case Agent::kLeft:
				//primero giro 90� 
				if (current_unit_patron_ == 0)
					direction_ = vo.LeftRotate90(direction_);
				//luego me muevo
				pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
				break;
			case Agent::kRight:
				//primero giro 90�
				if (current_unit_patron_ == 0)
					direction_ = vo.RightRotate90(direction_);
				//luego me muevo
				pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
				break;
			case Agent::kStop:
				//inicio un contador para estar parado 2 seg
				time_reached_ = false;
				current_time_ = ESAT::Time();
				break;
		}
		
	}
	else
	//me muevo durante 2 segundos en la direccion seleccionada
	//si es un Stop estare 2 seg parado
	{
		time_elapsed_ = ESAT::Time() - current_time_;
		if (time_elapsed_ >= 2000) //2 seg
		{
			time_elapsed_ = 0;
			time_reached_ = true;
			
		}
	}
	*/
}
void Agent::AwaitingMovment(int step)
{
	
}
/////////OTHER FUNCTIONS//////////
void Agent::ChoosePatron()
{
	/*
	//funcion que elige aleatoriamente uno de los tres patrones
	if (!patron_choose_)
	{
		patron_.clear();
		switch (rand() % 3)
		{
			case 0: 
			{
				for (int i = 0; i < 12; i++)
				{
					patron_.push_back(patron_1_[i]);
				}
				break;
			}
			case 1:
			{
				for (int i = 0; i < 15; i++)
				{
					patron_.push_back(patron_2_[i]);
				}
				break;
			}
			case 2:
			{
				for (int i = 0; i < 19; i++)
				{
					patron_.push_back(patron_3_[i]);
				}
				break;
			}
		}
	}
	*/	
}
void Agent::ResetDirection()
{
	/*
	//funcion que resetea la direccion del agente
	//para evitar que el patron empieze en una direccion equivocada
	//la direccion inicial es mirando a la derecha
	Point_2 p;
	p.x = pos_.x + speed_;
	p.y = pos_.y;
	direction_ = vo.CalcDirection(pos_, p);
	*/
}
bool Agent::SM_Agent(int id_agent_to_pursuit)
{
	/*
	//funcion que que devuielve true si estoy siguiendo al
	//agnte que me han pasado
	if (id_agent_to_pursuit == tracking_at_) return true;
	else return false;
	*/
	return false;
}
void Agent::PathToNavPoints() {
	
}
void Agent::OpenDoor() {
	float dist_to_door;
	if (pos_.x < 480) {
		dist_to_door = vo.CalcDistance(pos_, gm_->door_B_);
		if (dist_to_door < gm_->dist_to_use_door_) {
			gm_->cost_map.UseDoor('B', "Open");
			//state_type_ = kSearchDoor;
		}
	}
	else {
		dist_to_door = vo.CalcDistance(pos_, gm_->door_A_);
		if (dist_to_door < gm_->dist_to_use_door_) {
			gm_->cost_map.UseDoor('A', "Open");
			//state_type_ = kSearchDoor;
		}
	}
}
void Agent::CloseDoor() {
	float dist_to_door;
	if (pos_.x < 480) {
		dist_to_door = vo.CalcDistance(pos_, gm_->door_B_);
		if (dist_to_door < gm_->dist_to_use_door_) {
			gm_->cost_map.UseDoor('B', "Close");
			//state_type_ = kSearchDoor;
		}
	}
	else {
		dist_to_door = vo.CalcDistance(pos_, gm_->door_A_);
		if (dist_to_door < gm_->dist_to_use_door_) {
			gm_->cost_map.UseDoor('A', "Close");
			//state_type_ = kSearchDoor;
		}
	}
}
int Agent::SelecAdyacent(int in_room) {
	adyacent_rooms_.clear();
	adyacent_rooms_tmp_.clear();
	for (int i = in_room * 28; i < (in_room * 28) + 28; i++)
	{
		adyacent_rooms_.push_back(gm_->adyacence_matrix_[i]);
	}
	for (int i = 0; i < adyacent_rooms_.size(); i++)
	{
		if (adyacent_rooms_.at(i) == 1)
			adyacent_rooms_tmp_.push_back(i);
	}
	
	int tmp = rand() % adyacent_rooms_tmp_.size();

	return adyacent_rooms_tmp_.at(tmp);
	printf("");
}
void Agent::GoToNextRoom(int room) {
	 //recorro las puertas de las habitaciones contiguas hasta encontrar
	//la que es igual y la a�ado a mi path para ir hasta ella
	nav_points_.path_.clear();
	Point_2 door_a, door_b;
	int dist_a_b = 1000000;
	int dist_b_a = 1000000;
	int dist_tmp = 0;

	for (int i = 0; i < gm_->room_[in_room_].doors_.size(); i++)
	{
		for (int j = 0; j < gm_->room_[room].doors_.size(); j++)
		{
			dist_tmp = vo.CalcDistance(gm_->room_[in_room_].doors_[i],
				gm_->room_[room].doors_[j]);
			
			if (dist_tmp < dist_a_b) {
				door_a = gm_->room_[in_room_].doors_[i];
				dist_a_b = dist_tmp;
			}
			if (dist_tmp < dist_b_a) {
				door_b = gm_->room_[room].doors_[j];
				dist_b_a = dist_tmp;
			}
		}
	}
	nav_points_.Add(door_b);
	nav_points_.Add(door_a);

	printf("");

 }
bool Agent::CheckInside()
{
	if (agent_type_ == kSoldier) {
		//zona puerta B
		if (pos_.x > 130 && pos_.x < 175 &&
				pos_.y > 230 && pos_.y < 280) {
			
			return true;
		
		}
		//zona puerta A
		else if(pos_.x > 790 && pos_.x < 810 &&
						pos_.y > 190 && pos_.y < 375){
			return true;

		}	else if (pos_.x < 175 || pos_.x > 790) {
			return false;
		}
		else {
			if (pos_.y < 30 || pos_.y > 565) {
				return false;
			}
			else {
				return true;
			}

		}

	}
	
}
int Agent::InRoom()
{
	//recorro las habitaciones para saber en cual estoy
	for (int i = 0; i < 28; i++)
	{
		if (pos_.x > gm_->room_[i].upper_square_.x &&
			pos_.x < gm_->room_[i].lower_square_.x &&
			pos_.y > gm_->room_[i].upper_square_.y &&
			pos_.y < gm_->room_[i].lower_square_.y) {
			return i;
		 }
	}
	
	return -1;
}
