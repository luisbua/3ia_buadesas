
// project A Star
// cost_map.h
// author: Toni Barella
// Artificial Intelligence - 3VGP
// ESAT 2016/2017

#ifndef __COST_MAP__
#define __COST_MAP__

#include "ESAT_SDK/sprite.h"

#include"map.h"
#include "types.h"

#include <vector>
#include <cstdio>

using namespace ESAT;


typedef float Cost;

// Use one of the next declarations
typedef struct cell_s {
	Vec_2 position_;
	bool is_walkable_;
	Cost cost_;
} Cell;

class CostMap
{
public:
	
	/**
	* @brief Load Cost Maps from BMP or TXT files
	*/
	// Cost map can be a text file or an image file
	bool Load(const char *path);
	

	// Todo: Add setters / getters
	bool isWalkable(Vec_2 position);
	// Prints the cells of cost_map
	void Print(); 
	// Draws the cost map's image if available
	void Draw(int x = 0, int y = 0); 

	std::vector<Cell> cost_map_;
	int width_;
	int height_;

private:
	 SpriteHandle map_handle_;
	 FILE *pFile;
	 char buffer[1];

	 bool LoadTXT(const char *path);
	 bool LoadBMP(const char *path);
	 void CalcCellCostBMP();
};


#endif // __COST_MAP__