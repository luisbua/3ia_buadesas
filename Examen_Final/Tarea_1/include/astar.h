
// project A Star
// astar.h
// author: Toni Barella
// Artificial Intelligence - 3VGP
// ESAT 2016/2017

#ifndef __ASTAR__
#define __ASTAR__

#include "path.h"
#include "map.h"
#include "cost_map.h"
#include "types.h"

using namespace ESAT;

	class AStar
	{
	public:
		struct Node {
			Vec_2 position_;
			Vec_2 father_;
			float F_; //F = G + H
			float G_; //Cost
			float H_; //Heuristic
		};
		Path path_;
		
		/**
		* @brief 	Process the map (prepared for task 2) 
		*/
		bool PreProcess(CostMap *cost_map);
		/**
		* @brief Main function that calcualate A*
		*/
		bool GeneratePath(Vec_2 origin, Vec_2 destination, Path *path);
		/**
		* @brief Clean open_list, closed_list and node_successor
		*/
		void ResetData();

	private:
		
		Node node_goal_,node_start_,node_current_,node_swap_;
		std::vector<Node> open_list_;
		std::vector<Node> close_list_;
		std::vector<Node> node_successor_;
		CostMap* cost_map_;
		
		/**
		* @brief Return a heuristic value
		* two methods: Manhatan an Module
		*/
		float CalculateHeuristic(Node node);
		/**
		* @brief Travel from node_goal to node_start to add a path vector.
		*/
		void CalculatePath();
		/**
		* @brief Initcialize node_start and node_goal
		*/
		void FillNode(Node* node);
		/**
		* @brief Compare two pints and return true or false
		*/
		bool ComparePoint(Vec_2 a, Vec_2 b);
		/**
		* @brief Travel closed_list to find node with minor G
		*/
		int FindLowNodeCost();
		/**
		* @brief Return de G value from node_succesor
		*/
		float MoveCost(Node current, Node successor);

	};
#endif // __ASTAR__