#include "ESAT_SDK/window.h"
#include "ESAT_SDK\input.h"
#include "ESAT_SDK\draw.h"

#include "map.h"
#include "astar.h"
#include "cost_map.h"	
#include "path.h"

#include <stdio.h>

	void Init(Map *map, CostMap *costmap) {
		
		//Map 2
		map->Load("../../data/map_02_terrain.bmp");
		costmap->Load("../../data/map_02_cost.bmp");
		
		//Fuga de Colditz
		//map->Load("../../data/map4.bmp");
		//costmap->Load("../../data/map5.bmp");
		
		//Laberinto
		//map->Load("../../data/map_12_terrain.bmp");
		//costmap->Load("../../data/map_12_cost.txt");
	
}

	int ESAT::main(int argc, char **argv)
	{
		Map map;
		AStar astar;
		CostMap cost_map;
		Path path;

		bool click_start = false;
		bool click_end = false;
		

		ESAT::WindowInit(1024,1024);
		SpriteHandle green = SpriteFromFile("../../data/node_green.png");
		SpriteHandle red = SpriteFromFile("../../data/node.png");
		SpriteHandle arrow = SpriteFromFile("../../data/arrow.png");
		
		Init(&map, &cost_map);
		astar.PreProcess(&cost_map);
			
		int scale = map.width / cost_map.width_;

		Vec_2 origen = { -100,-100 };
		Vec_2 destino = { -100,-100 };
		while (ESAT::WindowIsOpened() &&
			!ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {
			ESAT::DrawBegin();
			
			map.Draw();
			
			DrawSprite(arrow, MousePositionX(), MousePositionY());
			DrawSprite(green, origen.x * scale, origen.y * scale);
			
		
			if (!click_start) {
				 if (MouseButtonDown(0)) {
					origen.x = round(MousePositionX() / scale);
					origen.y = round(MousePositionY() / scale);
					if (cost_map.isWalkable(origen))
					{
						printf("\nOrigen: X:%2.0f Y:%2.0f", origen.x, origen.y);
						click_start = true;
						astar.path_.path_.clear();
						
					}
					else printf("\n\nEscoge otro punto de ORIGEN\n\n");
				}
				

			}
			else if (!click_end) {
				if (MouseButtonDown(0)) {
					destino.x = round(MousePositionX() /scale);
					destino.y = round(MousePositionY() / scale);
					if (cost_map.isWalkable(destino))
					{
						printf("\nDestino: X:%2.0f Y:%2.0f", destino.x, destino.y);
						click_end = true;
						
					}
					else printf("\n\nEscoge otro punto de DESTINO\n\n");
				}
			}
			
			if (click_start && click_end) {
				astar.GeneratePath(origen, destino, &path);
				click_end = false;
				click_start = false;
				
				astar.ResetData();
			}
			astar.path_.Print(scale);
			
			
			//cost_map.Draw(0,0);
				
			
			ESAT::DrawEnd();
			ESAT::WindowFrame();
		}

			
		ESAT::WindowDestroy();
		return 0;
	
}//end namespace ESAT