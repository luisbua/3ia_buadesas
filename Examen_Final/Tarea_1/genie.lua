solution "3IA_AStar"

platforms {"x64", "x32"}
configurations {"Debug", "Release"}

location "build/3IA_AStar"
  project "3IA_AStar"
    language "C++"
    kind "consoleApp"

    files {"include/**.h","src/**.cpp"}
    includedirs {"include"}
    
    libdirs{"extern/lib/ESAT_SDK"}    
    includedirs{"extern/include"}

    configuration "Debug"
      flags {"Symbols"}
		links{ "ESAT_d"
		}
    configuration "Release"
      flags {"OptimizeSize"}
		links{ "ESAT"
		}
    configuration "windows"
      targetdir   "build/bin/windows"
      links { "imm32",
              "oleaut32",
              "winmm",
              "version",
              "OpenGL32"
            }

