#include "ESAT/input.h"
#include "ESAT/window.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "ESAT/sprite.h"
#include "agent.h"
#include "injured.h"

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <time.h>


ESAT::SpriteHandle soldier,doctor,injured;
const int num_soldiers = 10;
const int num_draw = 1;
//const int num_update = 10000;
const int IA_fixedStep = 40; //step in ms
double ct;
float Draw_time;
float IA_time;
int id_soldier = 0;

Agent* agent;
std::vector<Injured> soldiers;


namespace ESAT {
	
	void init_koala()
	{
		agent = new(Agent);
		
		for (int i = 0; i < num_soldiers; i++)
		{
			Injured a(agent);
			a.id_ = id_soldier;
			//printf("Creado nuevo soldado id: %d\n", i);
			id_soldier++;
			soldiers.push_back(a);
		}
		agent->TakeSoldiers(soldiers);
		
	}

	void TestProcess()
	{
		IA_time = Time();
		
		IA_time = Time() - IA_time;
		//printf("IA Time: %f\n", IA_time);
	}

	void Imput()
	{
		for (int i = 0; i < agent->injured_priority_.size(); i++)
		{
			agent->injured_priority_[i].ImInjured();
		}
		printf("");
	}
	void Update()
	{
		float accutime = Time() - ct;
		//printf("Acutime In: %f\n", accutime);
		//printf("\n");
		while (accutime >= IA_fixedStep)
		{
			agent->Thinking();
			//printf("Bucle IA: %.0f\n", ct);
			ct += IA_fixedStep;
			accutime = Time() - ct;
			//printf("Acutime Proces: %f\n", accutime);
		}
		//printf("\n");
		//printf("Acutime Out: %f\n", accutime);
	}
	void Draw()
	{
		ESAT::DrawBegin();
		ESAT::DrawClear(0, 0, 0);
		Draw_time = Time();
	
			for (int i = 0; i < num_soldiers; i++)
			{
				agent->Acting();
				if (!agent->injured_priority_[i].iminju_)
					DrawSprite(soldier, agent->injured_priority_[i].pos_.x, agent->injured_priority_[i].pos_.y);
				else
					DrawSprite(injured, agent->injured_priority_[i].pos_.x, agent->injured_priority_[i].pos_.y);
			}
			DrawSprite(doctor,agent->pos_.x,agent->pos_.y);
		Draw_time = Time() - Draw_time;
		//printf("Draw Time: %f\n", Draw_time);
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	

	int ESAT::main(int argc, char **argv)
	{

		ESAT::WindowInit(1000, 800);
		soldier = ESAT::SpriteFromFile("../../data/soldier1.png");
		doctor = ESAT::SpriteFromFile("../../data/doctor.png");
		injured = ESAT::SpriteFromFile("../../data/injured.png");
		init_koala();

		ct = Time();
		srand(time(NULL));
		
		while (ESAT::WindowIsOpened())
		{
			
			Imput();
			Update();
			Draw();

		}
		
		ESAT::WindowDestroy();

		return 0;
	}
}