#include "injured.h"
#include <stdlib.h>
#include <time.h>
#include "agent.h"
#include <math.h>

Injured::Injured()
{

}
Injured::Injured(Agent * agent) :
	iminju_(false),
	id_(0),
	distance_(0),
	urgency_(0),
	path_dificulty_(0),
	agent_(agent)
{
	pos_.x = rand() % 900 + 50;
	pos_.y = rand() % 700 + 50;
}
Injured::~Injured()
{

}
float Injured::CalcDistance()
{
	Vec2 aux;
	aux.x = pos_.x - agent_->pos_.x;
	aux.y = pos_.y - agent_->pos_.y;
	return sqrtf((aux.x * aux.x)+(aux.y + aux.y));
}
bool Injured::ImInjured()
{
	if (!iminju_)
	{
		rand_ = rand() % 1000;
		if (rand_ == 1)
		{
			iminju_ = true;
			CallMedic();
		}
		else iminju_ = false;
	}

	return iminju_;
}
void Injured::CallMedic()
{
	
	urgency_ = rand() % 100 + 1;
	path_dificulty_ = rand() % 3 + 1;
	printf("Soldado Herido id: %d\n",id_);
	
}