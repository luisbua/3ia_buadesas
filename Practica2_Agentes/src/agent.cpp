#include "agent.h"
#include <cmath>

Agent::Agent(): id_(0)
				
{
	pos_.x = 500;
	pos_.y = 400;
	center_.x = 500;
	center_.y = 400;
}

Agent::~Agent()
{

}
void Agent::Thinking()
{
	UpdateInjuredStatus();
	
}
void Agent::TakeSoldiers(std::vector<Injured>& v)
{
	injured_priority_ = v;
}
Vec2 Agent::CalcVector(Vec2 dest)
{
	Vec2 v;
	v.x = dest.x - pos_.x;
	v.y = dest.y - pos_.y;

	return v;
}
float Agent::CalcDistance(Vec2 v)
{
	float dist;
	dist = sqrt((v.x * v.x) + (v.y * v.y));
	return dist;
}
Vec2 Agent::CalcUnitVector(Vec2 v, float module)
{
	Vec2 aux;
	aux.x = (v.x / module) * 0.2;
	aux.y = (v.y / module) * 0.2;

	return aux;
}
void Agent::Move(Vec2 v)
{
	pos_.x += v.x ;
	pos_.y += v.y ;
	
}
void Agent::Acting()
{
	if (injured_priority_.front().priority_ > 0)
	{
		Vec2 aux, aux_dir;
		float aux_dis;
		aux_dir = CalcVector(injured_priority_.front().pos_);
		aux_dis = CalcDistance(aux_dir);
		aux = CalcUnitVector(aux_dir, aux_dis);
		Move(aux);
	  if (aux_dis < 5.0)
		{
			printf("Curado el Soldado: %d\n", injured_priority_.front().id_);
			injured_priority_.front().iminju_ = false;
			injured_priority_.front().urgency_ = 0;
			injured_priority_.front().priority_ = 0;
			UpdateInjuredStatus();
		}
	}
	else
	{
		Vec2 aux, aux_dir;
		float aux_dis;
		aux_dir = CalcVector(center_);
		aux_dis = CalcDistance(center_);
		aux = CalcUnitVector(aux_dir, aux_dis);
		Move(aux);
	}
}
void Agent::UpdateInjuredStatus()
{
	if (injured_priority_.size() > 0)
	{
		int z = 0;
		for (int w = 0; w < injured_priority_.size(); w++)
		{
			CalculatePriority(&injured_priority_[w]);
		}
		//Ordeno el vector por prioridades
		for (int k = 0; k < injured_priority_.size(); k++)
		{
			for (int i = 1; i < injured_priority_.size(); i++)
			{
				z = i - 1;
				
				if (injured_priority_[i].priority_ > injured_priority_[z].priority_)
				{
					inju_swap1_ = injured_priority_[i];
					inju_swap2_ = injured_priority_[z];
					injured_priority_[i] = inju_swap2_;
					injured_priority_[z] = inju_swap1_;
				
				}
			}
		}
	}
}


Injured* Agent::CalculatePriority(Injured *i)
{
	i->distance_ = CalcDistance(i->pos_);
	i->priority_ =
		(i->urgency_ * 3);
	
	return i;
}
