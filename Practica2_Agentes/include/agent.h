#pragma once

#include "injured.h"
#include "types.h"
#include <vector>

class Agent {

private:
	
	Injured *inju_swap_;
	Injured inju_swap1_;
	Injured inju_swap2_;
	
	Vec2 center_;


public:
	Agent();
	~Agent();

	std::vector<Injured> injured_priority_;

	void Thinking();
	void Acting();
	void UpdateInjuredStatus();
	Injured* CalculatePriority(Injured* i);
	float CalcDistance(Vec2 v);
	Vec2 CalcVector(Vec2 dest);
	Vec2 CalcUnitVector(Vec2 v, float module);
	void TakeSoldiers(std::vector<Injured>& v);
	void Move(Vec2 v);

	int id_;
	Vec2 pos_;

protected:

};




