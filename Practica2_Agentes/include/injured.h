#pragma once

#include "types.h"

class Agent;

class Injured {

public:

	Injured();
	Injured(Agent * agent);
	~Injured();
	bool ImInjured();
	void CallMedic();
	float CalcDistance();

	int id_;
	float priority_;
	float distance_;
	int urgency_;
	int path_dificulty_;
	bool iminju_;
	Vec2 pos_;
	
private:
	int rand_;
	Agent* agent_;
protected:
};