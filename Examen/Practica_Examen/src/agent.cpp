#include "agent.h"

Agent::Agent(){}
Agent::~Agent(){}

void Agent::Init(int id, AgentType ag, float x, float y)
{
	id_ = id;
	agent_type_ = ag;
	state_type_ = kWorking;
	dist_ = 1000;
	nearest_id_ = id;
	chase_dinstance_ = 80;
	flee_distance_ = 50;
	rest_distance_ = 150;
	tracking_at_ = id;
	tracking_me_ = id;
	speed_ = 0.025;
		
	switch (ag)
	{																		 
	case Agent::kSmall: speed_ *= 1.25f;
		break;
	case Agent::kNormal: speed_ *= 1.0f;
		break;
	case Agent::kHuge: speed_ *= 0.75f;
		break;
	default: printf("Agent Type Wrong\n");
		break;
	}
	resting_time_ = 0;
	//si no me han dado la posicion de salida 
	//elijo una aleatoria
	if (x == 0 && y == 0)
	{
		start_.x = rand() % 800 + 100;
		start_.y = rand() % 600 + 100;
	}
	else
	{
	 	start_.x = x;
		start_.y = y;
	}
	pos_ = start_;
	

	ResetDirection();
	
	radio_patrol_ = 100;
	reached_ = true;
	patron_choose_ = false;
	time_reached_ = true;
	length_unit_patron_ = 30;
	current_unit_patron_ = length_unit_patron_;
	
	//Puntos de navegacion para el movimiento determinista
	//Es un triangulo
	nav_point_reached_ = num_nav_points_;
	nav_points_[0] = { start_.x,start_.y - 100 };
	nav_points_[1] = { start_.x + 50,start_.y + 50 };
	nav_points_[2] = { start_.x - 50,start_.y + 50 };

	ChoosePatron();
	patron_point_reached_ = patron_.size();
}
/////////////UPDATE///////////////
void Agent::Update(int step)											  
{
	Mind(step);
	Body(step);
}
void Agent::Mind(int step)
{
	switch (state_type_)
	{
	case Agent::kWorking: Working(); break;
	case Agent::kChasing: Chasing(); break;
	case Agent::kFleeing: Fleeing(); break;
	case Agent::kResting: Resting(); break;
	default: printf("State Type Wrong\n");	break;
	}
}
void Agent::Body(int step)
{
	switch (move_type_)
	{
	case Agent::kDeterminist: DeterministMovment(step); break;
	case Agent::kRandom: RandomMovment(step);	break;
	case Agent::kTrack: TrackMovment(step);	break;
	case Agent::kPatron: PatronMovment(step); break;
	case Agent::kAwaiting: AwaitingMovment(step); break;
	default: printf("Move Type Wrong\n"); break;
	}
}
/////////////DRAW/////////////////
void Agent::Draw()
{
	switch (agent_type_)
	{
	case Agent::kSmall:
		ESAT::DrawSprite(gm_->s_small, pos_.x, pos_.y);
		break;
	case Agent::kNormal:
		ESAT::DrawSprite(gm_->s_normal, pos_.x, pos_.y);
		break;
	case Agent::kHuge:
		ESAT::DrawSprite(gm_->s_huge, pos_.x, pos_.y);
		break;
	default:
		printf("Draw Wrong\n");
	}
}
///////////////MIND///////////////
void Agent::Working()
{
	//elijo el tipo de movimiento por defecto	
	switch (agent_type_)
		{
		case Agent::kSmall: move_type_ = kPatron; break;
		case Agent::kNormal: move_type_ = kRandom; break;
		case Agent::kHuge: move_type_ = kDeterminist; break;
		default: printf("Agent Type Wrong\n");	break;
		}
																 	
	nearest_id_ = id_;
	dist_ = 1000;
	//calculo el agente mas cercano
	for (int i = 0; i < gm_->num_agents; i++)
	{
		if (gm_->agent_.at(i)->id_ != id_)
		{
			aux_dist_ = vo.CalcDistance(pos_, gm_->agent_.at(i)->pos_);
			if (dist_ > aux_dist_)
			{
				dist_ = aux_dist_;
				nearest_id_ = i;
			}
		}
	}
	//si distancia < 50 y soy menor HUYO
	if (dist_ < flee_distance_ &&
		agent_type_ < gm_->agent_.at(nearest_id_)->agent_type_)
	{
		tracking_me_ = nearest_id_;
		state_type_ = kFleeing;
	}
	//sino, si la distancia < 80 y soy mayor PERSIGO
	else if (dist_ < chase_dinstance_ &&
		agent_type_ > gm_->agent_.at(nearest_id_)->agent_type_)
	{
		tracking_at_ = nearest_id_;
		switch (agent_type_)
		{
		case Agent::kSmall:
			printf("Small can't pursuit\n");
			break;
		case Agent::kNormal:
			//mando un mensaje al resto de agentes Normal para ver si 
			//alguno esta persiguiendo a mi objetivo, si no lo persigue nadie 
			//entonces lo puedo perseguir
			if (!gm_->messengeragent_->SM_Messenger(id_, tracking_at_,
				"Perseguir", MessengerAgent::kNormalPursuit))
			{
				state_type_ = kChasing;
			}
			break;
		case Agent::kHuge:
			//mando un mensaje al resto de agentes Huge para ver si 
			//alguno esta persiguiendo a mi objetivo, si no lo persigue nadie 
			//entonces lo puedo perseguir
			if (!gm_->messengeragent_->SM_Messenger(id_, tracking_at_,
				"Perseguir", MessengerAgent::kHugePursuit))
			{
				state_type_ = kChasing;
			}
			break;
		default:
			printf("Pursuit Wrong\n");
			break;
		}
	}
}
void Agent::Chasing()
{
	//selecciono el tipo de movimiento para Chasing
	move_type_ = kTrack;
	//claculo distancia con el objetivo que persigo
	dist_ = vo.CalcDistance(pos_, gm_->agent_.at(tracking_at_)->pos_);
	if (dist_ > rest_distance_)
	{
		state_type_ = kResting;
		resting_time_ = ESAT::Time();
	}
}
void Agent::Fleeing()
{		
	//selecciono el tipo de movimiento para	Fleeing
	move_type_ = kTrack;
	//claculo distancia con el agente que me persigue
	dist_ = vo.CalcDistance(pos_, gm_->agent_.at(tracking_me_)->pos_);
	if (dist_ > rest_distance_)
	{
		state_type_ = kResting;
		resting_time_ = ESAT::Time();
		tracking_me_ = id_;
	}
}
void Agent::Resting()
{
	//selecciono el tipo de movimiento para	Resting
	move_type_ = kAwaiting;
	
	nearest_id_ = id_;
	dist_ = 1000;
	for (int i = 0; i < gm_->num_agents; i++)
	{
		if (gm_->agent_.at(i)->id_ != id_)
		{
			//claculo distancia con todos los agentes que me persiguen
			//Nota: ahora solo me puede perseguir uno de cada tipo
			aux_dist_ = vo.CalcDistance(pos_, gm_->agent_.at(i)->pos_);
			if (dist_ > aux_dist_)
			{
				dist_ = aux_dist_;
				nearest_id_ = i;
			}
		}
	}
	//si distancia < 50 y soy menor HUYO
	if (dist_ < flee_distance_ &&
		agent_type_ < gm_->agent_.at(nearest_id_)->agent_type_)
	{
		tracking_me_ = nearest_id_;
		state_type_ = kFleeing;
	}
	//si nadie me persigue descanso 4 seg antes de poder actuar de nuevo
	if (ESAT::Time() - resting_time_ > 4000)
	{
		state_type_ = kWorking;
		resting_time_ = 0;
		reached_ = true;
		ResetDirection();
	}

}
///////////////BODY///////////////
void Agent::DeterministMovment(int step)
{
	//Si alcanzo el punto de navegacion selcciono
	//el siguiente punto de navegacion
	if (reached_)
	{
		if (nav_point_reached_ < num_nav_points_ - 1) nav_point_reached_++;
		else nav_point_reached_ = 0;

		dest_ = nav_points_[nav_point_reached_];
		direction_ = vo.CalcDirection(pos_, dest_);
		reached_ = false;
	}
	else
	//me muevo hacia el punto de navegacion
	{
		pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
		if (vo.CalcDistance(pos_, dest_) <= 5.5f)
		{
			reached_ = true;
		}
	}
	
}
void Agent::RandomMovment(int step)
{
	//Si alcanzo el punto de navegacion selcciono
	//el siguiente punto de navegacion de forma aleatoria
	//dentro de un radio
	if (reached_)
	{
		dest_.x = rand() % 100 + (start_.x - radio_patrol_);
		dest_.y = rand() % 100 + (start_.y - radio_patrol_);
		direction_ = vo.CalcDirection(pos_, dest_);
		reached_ = false;
	}
	else
	//me muevo hacia el punto de navegacion
	{
		pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
		if (vo.CalcDistance(pos_, dest_) <= 5.5f)
		{
			reached_ = true;
		}
	}
}
void Agent::TrackMovment(int step)
{
	//si estoy huyendo
	if (state_type_ == kFleeing)
	{
		//claculo la direccion del que me persigue y
		//huyo en su misma direccion
		direction_ = 
			vo.CalcDirection(gm_->agent_.at(tracking_me_)->pos_,pos_);
		
		pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
	
	}
	//si estoy persiguiendo
	if (state_type_ == kChasing)
	{
		//calculo la direccion hacia mi objetivo
		direction_ =
			vo.CalcDirection(pos_,gm_->agent_.at(tracking_at_)->pos_);
		
		pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
		
	}
}
void Agent::PatronMovment(int step)
{
	//si han pasado 2 seg leo la nueva instruccion
	if (time_reached_)
	{
		//Dentro de cada instruccion he de moverme durante
		//un tiempo, cada frame incremento current_unit_patron
		//para saber en que parte de la instruccion estoy,
		//si estoy en 0 es que estoy en el inicio de la intruccion
		if (current_unit_patron_ < length_unit_patron_ && 
			  patron_[patron_point_reached_] != kStop)
		{
			current_unit_patron_++;
		}
		else
		{
			current_unit_patron_ = 0;
			//voy recorriendo las instrucciones del patron
			if (patron_point_reached_ < patron_.size() - 1)
				patron_point_reached_++;
			else
			{
				patron_point_reached_ = 0;
				ResetDirection();
			}
		}
		//Ejecuto la accion segun la instruccion recibida 		
		switch (patron_[patron_point_reached_])
		{
			case Agent::kForward:
				pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
				break;
			case Agent::kLeft:
				//primero giro 90� 
				if (current_unit_patron_ == 0)
					direction_ = vo.LeftRotate90(direction_);
				//luego me muevo
				pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
				break;
			case Agent::kRight:
				//primero giro 90�
				if (current_unit_patron_ == 0)
					direction_ = vo.RightRotate90(direction_);
				//luego me muevo
				pos_ = vo.CalcNewPosition(pos_, direction_,step, speed_);
				break;
			case Agent::kStop:
				//inicio un contador para estar parado 2 seg
				time_reached_ = false;
				current_time_ = ESAT::Time();
				break;
		}
		
	}
	else
	//me muevo durante 2 segundos en la direccion seleccionada
	//si es un Stop estare 2 seg parado
	{
		time_elapsed_ = ESAT::Time() - current_time_;
		if (time_elapsed_ >= 2000) //2 seg
		{
			time_elapsed_ = 0;
			time_reached_ = true;
			
		}
	}
}
void Agent::AwaitingMovment(int step)
{
	//No hago nada de movimiento
}
/////////OTHER FUNCTIONS//////////
void Agent::ChoosePatron()
{
	//funcion que elige aleatoriamente uno de los tres patrones
	if (!patron_choose_)
	{
		patron_.clear();
		switch (rand() % 3)
		{
			case 0: 
			{
				for (int i = 0; i < 12; i++)
				{
					patron_.push_back(patron_1_[i]);
				}
				break;
			}
			case 1:
			{
				for (int i = 0; i < 15; i++)
				{
					patron_.push_back(patron_2_[i]);
				}
				break;
			}
			case 2:
			{
				for (int i = 0; i < 19; i++)
				{
					patron_.push_back(patron_3_[i]);
				}
				break;
			}
		}
	}
}
void Agent::ResetDirection()
{
	//funcion que resetea la direccion del agente
	//para evitar que el patron empieze en una direccion equivocada
	//la direccion inicial es mirando a la derecha
	Point2 p;
	p.x = pos_.x + speed_;
	p.y = pos_.y;
	direction_ = vo.CalcDirection(pos_, p);
}
bool Agent::SM_Agent(int id_agent_to_pursuit)
{
	//funcion que que devuielve true si estoy siguiendo al
	//agnte que me han pasado
	if (id_agent_to_pursuit == tracking_at_) return true;
	else return false;
}
															