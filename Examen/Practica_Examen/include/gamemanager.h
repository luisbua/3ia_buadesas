#pragma once

#include "ESAT/input.h"
#include "ESAT/window.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "ESAT/sprite.h"

#include <stdio.h>
#include <time.h>
#include <vector>
#include <memory>

#include "types.h"
//#include "agent.h"
class Agent;
class MessengerAgent;


class GameManager
{
private:
	static GameManager *instance_;
	GameManager();
	~GameManager();
public:
	
	static GameManager* Instance() {
		if (!instance_)
		{
			instance_ = new GameManager();
		}
		
		return instance_;
	}
	void Init();
	
	//numero de aggentes segun tipo
	static const int num_small = 10;
	static const int num_normal = 5;
	static const int num_huge = 2;
	static const int num_agents = num_small + num_normal + num_huge;
	//ancho y alto de la ventana
	const int width = 1000;
	const int height = 800;

	bool window_closed;
	
	//step in ms, 40 ms = 25 fps
	const int IA_fixedStep = 40; 
	
	double ct;
	float Draw_time;
	
	int agent_vector_position_;
	float real_time_to_update_;
	
	//vector de agentes
	std::vector<Agent*> agent_;
	//MessengerAgent
	MessengerAgent *messengeragent_;

	//sprites para pintar
	ESAT::SpriteHandle s_small;
	ESAT::SpriteHandle s_normal;
	ESAT::SpriteHandle s_huge;
};
