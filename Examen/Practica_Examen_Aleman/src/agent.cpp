#include "agent.h"

Agent::Agent(){}
Agent::~Agent(){}

void Agent::Init(int id, float x, float y)
{
	id_ = id;
	
	pos_.x = x;
	pos_.y = y;

	see_ = false;
	hear_ = false;
	dead_ = false;
	idle_time_ = 0; 
	max_time_idle_ = 3000;//3 seg;
	max_time_patrol_ = 6000;//6 seg
	atack_distance_ = 100;
	chase_distance_ = 150;
	dist_ = 200;
	enemy_life_ = 50;
	actual_time_ = ESAT::Time();
	state_type_ = kIdle;

}
/////////////UPDATE///////////////
void Agent::Update(int step)											  
{
	Mind(step);
	Body(step);
}
void Agent::Mind(int step)
{
	switch (state_type_)
	{
	case Agent::kIdle: Idle(step); break;
	case Agent::kPatrol: Patrol(); break;
	case Agent::kChase: Chase(); break;
	case Agent::kAtack: Atack(); break;
	case Agent::kSearch: Search(); break;
	case Agent::kDead: Dead(); break;
	default: printf("State Type Wrong\n");	break;
	}
}
void Agent::Body(int step)
{
	
}
/////////////DRAW/////////////////
void Agent::Draw()
{
	
}
///////////////MIND///////////////
void Agent::Idle(int step)
{
	printf("IDLE\n");
	//a los 3 segundos de Idle paso a patrullar
	idle_time_ = ESAT::Time() - actual_time_;
	if (idle_time_ > max_time_idle_)
	{
		state_type_ = kPatrol;
		actual_time_ = ESAT::Time();
	}

	//si oigo al intruso
	if (rand() % 150 == 1) hear_ = true;
	if (hear_) state_type_ = kSearch;
	//si veo al intruso
	if (rand() % 150 == 1) see_ = true;
	if (see_) state_type_ = kChase;
	//si el intruso me mata
	if (rand() % 10000 == 1) dead_ = true;
	if (dead_)state_type_ = kDead;

	

}
void Agent::Patrol()
{
	printf("PATROLL\n");
	//a los 6 segundos de patrullar paso a idle
	idle_time_ = ESAT::Time() - actual_time_;
	if (idle_time_ > max_time_patrol_)
	{
		state_type_ = kIdle;
		actual_time_ = ESAT::Time();
	}

	//si oigo al intruso
	if (rand() % 100 == 1) hear_ = true;
	if (hear_) state_type_ = kSearch;
	//si veo al intruso
	if (rand() % 100 == 1) see_ = true;
	if (see_) state_type_ = kChase;
	//si el intruso me mata
	if (rand() % 800 == 1) dead_ = true;
	if (dead_)state_type_ = kDead;

	

}
void Agent::Chase()
{
	printf("CHASE\n");
	//si dejo de ver al intruso
	if (rand() % 100 == 1) see_ = false;
	//si ni lo veo ni lo oigo
	if (!see_ && !hear_)
	{
		state_type_ = kPatrol;
		actual_time_ = ESAT::Time();
	}
	
	//si oigo al intruso
	if (rand() % 50 == 1) hear_ = true;
	//si no loveo pero lo oigo
	if (!see_ && hear_) state_type_ = kSearch;

	//Mientras lo veo lo persigo y me acerco
	if (see_)
	{
		dist_--;
		//si me acerco lo suficiente le ataco
		if (dist_ <= atack_distance_)
		{
			state_type_ = kAtack;
		}
	}
	
	//si el intruso me mata
	if (rand() % 10000 == 1) dead_ = true;
	if (dead_)state_type_ = kDead;

	
}
void Agent::Atack()
{
	printf("ATACK\n");
	//probabilidad de que se aleje el objetivo
	if (rand() % 50 == 1)
	{
		dist_ += 10;
	}
		//si me alejo lo suficiente lo persigo de nuevo
	if (dist_ > chase_distance_) state_type_ = kChase;
	
	//si dejo de verlo pero lo oigo
	if (rand() % 50 == 1) see_ = false;
	if (rand() % 10 == 1) hear_ = true;
	if (!see_ && hear_)
	{
		state_type_ = kSearch;
	}
	//disparo al enenmigo (25 dps)
	enemy_life_--;
	
	//si mato al enenmigo patrullo de nuevo
	if (enemy_life_ <= 0)
	{
		state_type_ = kPatrol;
		actual_time_ = ESAT::Time();
		
		printf("////////////////////////////\n");
		printf("////////////////////////////\n");
		printf("////////////////////////////\n");
		printf("///////ENEMIGO MUERTO///////\n");
		printf("////////////////////////////\n");
		printf("////////////////////////////\n");
		printf("////////////////////////////\n");
		
		//reseteo las variables para un nuevo objetivo
		see_ = false;
		hear_ = false;
		dist_ = 200;
		enemy_life_ = 100;

	}
	
	//si el intruso me mata
	if (rand() % 100 == 1) dead_ = true;
	if (dead_)state_type_ = kDead;

	
}
void Agent::Search()
{
	printf("SEARCH\n");
	//si lo veo lo persigo
	if (rand() % 50 == 1) see_ = true;
	if (see_) state_type_ = kChase;

	//si dejo de oirlo patrullo de nuevo
	if (rand() % 100 == 1) hear_ = false;
	if (!see_ && !hear_)
	{
		state_type_ = kPatrol;
		actual_time_ = ESAT::Time();
	}

	//si el intruso me mata
	if (rand() % 10000 == 1) dead_ = true;
	if (dead_)state_type_ = kDead;
}
void Agent::Dead()
{
	system("cls");
	printf("////////////////////////////\n");
	printf("////////////////////////////\n");
	printf("////////////////////////////\n");
	printf("////////MUERTO ESTOY////////\n");
	printf("////////////////////////////\n");
	printf("////////////////////////////\n");
	printf("////////////////////////////\n");

	
}
///////////////BODY///////////////

															