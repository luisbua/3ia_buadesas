solution "IA_buadesas"

platforms {"x64", "x32"}
configurations {"Debug", "Release"}

location "build/IA_buadesas"
  project "IA_buadesas"
    language "C++"
    kind "consoleApp"

    files {"include/**.h","src/**.cpp"}
    libdirs{"extern/bin/"}

    includedirs {"include"}
    includedirs{"extern/include"}

    configuration "Debug"
      flags {"Symbols"}

    configuration "Release"
      flags {"OptimizeSize"}

    configuration "windows"
      targetdir   "build/bin/windows"
      links { "imm32",
              "oleaut32",
              "winmm",
              "version",
              "OpenGL32",
              "ESAT_d"}

