#include "ESAT/input.h"
#include "ESAT/window.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "ESAT/sprite.h"

#include <stdlib.h>
#include <stdio.h>


ESAT::SpriteHandle sprite;
const int num_koala = 100;
const int num_draw = 1000;
const int num_update = 10000;
const int IA_fixedStep = 40; //step in ms
double ct;
float Draw_time;
float IA_time;

namespace ESAT {
	
	struct vec2
	{
		int x;
		int y;
	} pos[num_koala];

	void init_koala()
	{
		for (int i = 0; i < num_koala; i++)
		{
			pos[i].x = rand() % 1000;
			pos[i].y = rand() % 800;

		}
	}

	void TestProcess()
	{
		IA_time = Time();
		for (int k = 0; k < num_update; k++)
		{
			for (int i = 0; i < num_koala; i++)
			{
				pos[i].x++;
				pos[i].y++;

				if (pos[i].x > 1000) pos[i].x = 0;
				if (pos[i].y > 800) pos[i].y = 0;

			}
		}
		IA_time = Time() - IA_time;
		printf("IA Time: %f\n", IA_time);
	}

	void Imput()
	{

	}
	void Update()
	{
		float accutime = Time() - ct;
		printf("Acutime In: %f\n", accutime);
		printf("\n");
		while (accutime >= IA_fixedStep)
		{
			TestProcess();
			printf("Bucle IA: %.0f\n", ct);
			ct += IA_fixedStep;
			accutime = Time() - ct;
			printf("Acutime Proces: %f\n", accutime);
		}
		printf("\n");
		printf("Acutime Out: %f\n", accutime);
	}
	void Draw()
	{
		ESAT::DrawBegin();
		ESAT::DrawClear(0, 0, 0);
		Draw_time = Time();
		for (int k = 0; k < num_draw; k++)
		{
			for (int i = 0; i < num_koala; i++)
			{
				ESAT::DrawSprite(sprite, pos[i].x, pos[i].y);
			}
		}
		Draw_time = Time() - Draw_time;
		printf("Draw Time: %f\n", Draw_time);
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	

	int ESAT::main(int argc, char **argv)
	{

		ESAT::WindowInit(1000, 800);
		sprite = ESAT::SpriteFromFile("../../data/koala.jpg");
		init_koala();

		ct = Time();
		
		while (ESAT::WindowIsOpened())
		{
			
			Imput();
			Update();
			Draw();

		}
		
		ESAT::WindowDestroy();

		return 0;
	}
}