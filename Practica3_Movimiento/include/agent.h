#pragma once

#include "types.h"

class Agent {

private:
	


public:
	Agent();
	~Agent();
	
	virtual void Update(int step)  = 0;
	
	int id_;
	Vec2 pos_;

protected:
	
	Vec2 CalcVector(Point2 start, Point2 dest);
	float CalcModule(Vec2 v);
	Vec2 CalcUnitVector(Vec2 v, float module, float scale);
	Point2 CalcNewPosition(Point2 a, Vec2 b);

};




