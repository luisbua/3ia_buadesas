#pragma once

#include "agent.h"
#include "soldier.h"

class Medic : public Agent
{
public:
	Medic();
	~Medic();

	void Init(int id, std::vector<Soldier>* v);
	void Update(int step);
	void Mind(int step);
	void Body(int step);
	
	int id_;
	Point2 pos_;
private:

	void CalcPriority(Soldier * s);
	void RandomMovement();
	void AidMovement();

	Point2 dest_, start_;
	Vec2 direction_;
	float radio_patrol_;
	bool reached_;
	int injured_to_aid_;
	int num_of_injured_;
	static const int num_nav_points_ = 3;
	Point2 nav_points_[num_nav_points_];
	int nav_point_reached_;
	std::vector<Soldier>* injured_soldiers_;
	float speed_;
	enum MoveType {
		kaid,
		krandom
	};
	MoveType move_type_;
	
protected:
};