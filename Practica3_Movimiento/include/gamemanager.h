#pragma once

#include "ESAT/input.h"
#include "ESAT/window.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "ESAT/sprite.h"

#include <stdio.h>
#include <time.h>
#include <vector>

#include "soldier.h"
#include "medic.h"

class GameManager
{
private:
	static GameManager *instance_;
	GameManager();
	~GameManager();
public:
	
	static GameManager* Instance() {
		if (!instance_)
		{
			instance_ = new GameManager();
		}
		
		return instance_;
	}
	void Init();
	
	const int num_soldiers = 10;
	const int num_draw = 1;

	const int width = 1000;
	const int height = 800;

	//const int num_update = 10000;
	const int IA_fixedStep = 40; //step in ms
	double ct;
	float Draw_time;
	float IA_time;
	int id_soldier = 0;

	std::vector<Soldier> soldiers;
	Medic doctor;

	ESAT::SpriteHandle s_sol; 
	ESAT::SpriteHandle s_doc; 
	ESAT::SpriteHandle s_inju;

};
