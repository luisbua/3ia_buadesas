#pragma once

#include <cmath>
#include <stdio.h>
#include <time.h>
#include <vector>

struct Vec2 {
	float x, y;
};

struct Point2 {
	float x, y;
};

