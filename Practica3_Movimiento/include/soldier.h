#pragma once

#include "agent.h"

class Soldier : public Agent {

public:

	Soldier();
	~Soldier();

	void Init(int id);
	void Update(int step);
	void Mind(int step);
	void Body(int step);
	void Healed();
	void ImShooted();
	 
	int id_;
	Point2 pos_;
	bool imshooted_;
	int damage;
	float priority_;
	enum MoveType {
		kpath,
		krandom,
		kstopped	
	};
	MoveType move_type_;
	MoveType CalcKindOfMovment();
	enum StateType {
		kpatroll,
		kinjured,
		kdie
	};
	StateType state_type_;

private:
	
	void PathMovement();
	void RandomMovement();
	void StoppedMovment();
	void Patroll();
	void Injured();
	void Die();
	
	
	
	Point2 dest_, start_;
	Vec2 direction_;
	float radio_patrol_;
	bool change_kind_movment;
	bool reached_;
	
	
	static const int num_nav_points_ = 3;
	Point2 nav_points_[num_nav_points_];
	int nav_point_reached_;
protected:
};