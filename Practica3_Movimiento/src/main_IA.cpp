#include "gamemanager.h"
#include <stdlib.h>

namespace ESAT {
	
	///////Bucle del START///////

	void Imput()
	{
		GameManager* gm_ = GameManager::Instance();
		for (int i = 0; i < gm_->soldiers.size(); i++)
		{
			gm_->soldiers[i].ImShooted();
		}
	}
	void Update(int step)
	{
		GameManager* gm_ = GameManager::Instance();
		for (int i = 0; i < gm_->soldiers.size(); i++)
		{
			gm_->soldiers[i].Update(step);
		}
		gm_->doctor.Update(step);
				
	}


	void Draw()
	{
		GameManager* gm_ = GameManager::Instance();
		ESAT::DrawBegin();
		ESAT::DrawClear(0, 0, 0);
		gm_->Draw_time = Time();
	
		for (int i = 0; i < gm_->soldiers.size(); i++)
		{
		
			if (!gm_->soldiers[i].imshooted_)
				DrawSprite(gm_->s_sol, gm_->soldiers[i].pos_.x, gm_->soldiers[i].pos_.y);
			else
				DrawSprite(gm_->s_inju, gm_->soldiers[i].pos_.x, gm_->soldiers[i].pos_.y);
		}
			
		DrawSprite(gm_->s_doc, gm_->doctor.pos_.x, gm_->doctor.pos_.y);
		gm_->Draw_time = Time() - gm_->Draw_time;
		//printf("Draw Time: %f\n", Draw_time);
		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}

	///////Funciones principales////////////

	void Init()
	{
		GameManager* gm_ = GameManager::Instance();
		
		for (int i = 0; i < gm_->num_soldiers; i++)
		{
			Soldier a;
			a.Init(i);
			gm_->soldiers.push_back(a);
		}
		gm_->doctor.Init(1, &gm_->soldiers);
	}
	void Start()
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->ct = Time();
		srand(time(NULL));

		while (ESAT::WindowIsOpened())
		{
			Imput();
			float accutime = Time() - gm_->ct;

			while (accutime >= gm_->IA_fixedStep)
			{
				Update(gm_->IA_fixedStep);
				printf("update\n");
				gm_->ct += gm_->IA_fixedStep;
				accutime = Time() - gm_->ct;
			}
			printf("draw\n");
			Draw();
		}
	}
	void ShutDown()
	{
		GameManager* gm_ = GameManager::Instance();
	}

	//////////////Main//////////////
	int ESAT::main(int argc, char **argv)
	{
		ESAT::WindowInit(1000,800);
		GameManager* gm_ = GameManager::Instance();
		gm_->Init();
				
		Init(); 
		Start();
		ShutDown();
			
		ESAT::WindowDestroy();
		return 0;
	}
}//end namespace ESAT