#include "medic.h"

Medic::Medic()
{

}
Medic::~Medic()
{

}
void Medic::Init(int id, std::vector<Soldier>* v)
{
	start_.x = 500;
	start_.y = 400;
	reached_ = true;
	radio_patrol_ = 450;
	pos_ = start_;
	id_ = id;
	direction_.x = 0;
	direction_.y = 0;
	move_type_ = krandom;
	speed_ = 4; //1-4
	injured_soldiers_ = v;
	injured_to_aid_ = 0;
	num_of_injured_ = 0;
}
void Medic::Update(int step)
{
	Mind(step);
	Body(step);
}
void Medic::Mind(int step)
{
	num_of_injured_ = 0;
	for (int i = 0; i < injured_soldiers_->size(); i++)
	{
		if (injured_soldiers_->at(i).imshooted_)
		{
			num_of_injured_++;
			CalcPriority(&injured_soldiers_->at(i));
			if (injured_soldiers_->at(i).priority_ >
				injured_soldiers_->at(injured_to_aid_).priority_)
			{
				injured_to_aid_ = injured_soldiers_->at(i).id_;
			}
			reached_ = true;
		}
	}

	if (num_of_injured_ <= 0) RandomMovement();
	else AidMovement();
}
void Medic::Body(int step)
{
	if (CalcModule(CalcVector(pos_, dest_)) <= 5.5f)
	{
		reached_ = true;

	}
	else
	{
		pos_ = CalcNewPosition(pos_, direction_);

	}
}

void Medic::CalcPriority(Soldier * s)
{
	s->priority_ = (s->damage * 3) + 0;
								 //(CalcModule(CalcVector(pos_, s->pos_)) * 2);
}
void Medic::RandomMovement() 
{
	Vec2 aux;
	if (reached_)
	{
		dest_.x = rand() % 900 + (start_.x - radio_patrol_);
		dest_.y = rand() % 700 + (start_.y - radio_patrol_ + 100);
		aux = CalcVector(pos_, dest_);
		direction_ = CalcUnitVector(aux, CalcModule(aux), speed_);
		reached_ = false;
	}
}
void Medic::AidMovement()
{
	Vec2 aux;
	float aux_mod;
	if (reached_)
	{
		dest_ = injured_soldiers_->at(injured_to_aid_).pos_;
		aux = CalcVector(pos_, dest_);
		aux_mod = CalcModule(aux);
		if (aux_mod < 5.5f)
		{
			injured_soldiers_->at(injured_to_aid_).Healed();
			reached_ = true;
		}
		else
		{
			direction_ = CalcUnitVector(aux, aux_mod, speed_ * 3);
			reached_ = false;
		}
	}
}