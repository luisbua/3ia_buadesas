#include "soldier.h"


Soldier::Soldier()
{
	//printf("Soldado creado\n");
	
}

Soldier::~Soldier()
{

}

void Soldier::Init(int id)
{
	
	start_.x = rand() % 800 + 100;
	start_.y = rand() % 600 + 100;
	radio_patrol_ = 100;
	imshooted_ = false;
	reached_ = true;
	pos_ = start_;
	id_ = id;
	direction_.x = 0;
	direction_.y = 0;
	damage = 0;
	priority_ = 0;
	change_kind_movment = true;
		
	move_type_ = kstopped;
	state_type_ = kpatroll;

	nav_point_reached_ = num_nav_points_;
	nav_points_[0] = { start_.x,start_.y - 100 };
	nav_points_[1] = { start_.x + 50,start_.y + 50 };
	nav_points_[2] = { start_.x - 50,start_.y + 50 };

	
}

void Soldier::Update(int step)
{
	Mind(step);
	Body(step);
}

void Soldier::Mind(int step)
{
	switch (state_type_)
		{
		case kpatroll: Patroll(); break;
		case kinjured: Injured();	break;
		case kdie: Die(); break;
		default: Patroll(); break;
		}
}

void Soldier::Body(int step)
{
	switch (move_type_)
	{
	case kpath:PathMovement(); break; //Vector de puntos
	case krandom:RandomMovement();	break;
	case kstopped:StoppedMovment(); break;
	default: PathMovement();  break;
	}
}

void Soldier::PathMovement()
{
	Vec2 aux;
	if (reached_)
	{
		if (nav_point_reached_ < num_nav_points_ - 1) nav_point_reached_++;
		else nav_point_reached_ = 0;

		dest_ = nav_points_[nav_point_reached_];

		aux = CalcVector(pos_, dest_);
		direction_ = CalcUnitVector(aux, CalcModule(aux), 1);
		reached_ = false;
	}
	else
	{
		pos_ = CalcNewPosition(pos_,direction_);
		if (CalcModule(CalcVector(pos_, dest_)) <= 5.5f)
		{
			reached_ = true;
		}
	}
}
void Soldier::RandomMovement()
{
	Vec2 aux;
	if (reached_)
	{
		dest_.x = rand() % 100 + (start_.x - radio_patrol_);
		dest_.y = rand() % 100 + (start_.y - radio_patrol_);
		aux = CalcVector(pos_, dest_);
		direction_ = CalcUnitVector(aux, CalcModule(aux), 1);
		reached_ = false;
	}
	else
	{
		pos_ = CalcNewPosition(pos_, direction_);
		if (CalcModule(CalcVector(pos_, dest_)) <= 5.5f)
		{
			reached_ = true;
		}
	}
}
void Soldier::StoppedMovment()
{
	dest_ = pos_;
}
void Soldier::ImShooted()
{
	if (!imshooted_)
	{
		if (rand() % 10000 == 1)
		{
			imshooted_ = true;
			damage = rand() % 1000 + 500;
			state_type_ = kinjured;
				
		}
	}
}
Soldier::MoveType Soldier::CalcKindOfMovment()
{
	switch (rand() % 2)
	{
	case 0: move_type_ = kpath; break;
	case 1: move_type_ = krandom; break;
	}
	return move_type_;
}
void Soldier::Patroll()
{
	if (change_kind_movment)
	{
		CalcKindOfMovment();
		change_kind_movment = false;
	}
}
void Soldier::Injured()
{
	move_type_ = kstopped;
}
void Soldier::Die()
{

}
void Soldier::Healed()
{
	imshooted_ = false;
	priority_ = 0;
	change_kind_movment = true;
	state_type_ = kpatroll;
}