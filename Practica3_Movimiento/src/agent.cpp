#include "agent.h"
#include <cmath>

Agent::Agent(): id_(0)
				
{
	pos_.x = 0;
	pos_.y = 0;

}

Agent::~Agent()
{

}

Vec2 Agent::CalcVector(Point2 start, Point2 dest)
{
	Vec2 v;
	v.x = dest.x - start.x;
	v.y = dest.y - start.y;

	return v;
}
float Agent::CalcModule(Vec2 v)
{
	float dist;
	dist = sqrt((v.x * v.x) + (v.y * v.y));
	return dist;
}
Vec2 Agent::CalcUnitVector(Vec2 v, float module, float scale = 1)
{
	Vec2 aux;
	aux.x = (v.x / module) * scale;
	aux.y = (v.y / module) * scale;

	return aux;
}
Point2 Agent::CalcNewPosition(Point2 a, Vec2 b)
{
	a.x += b.x;
	a.y += b.y;
	return a;
}