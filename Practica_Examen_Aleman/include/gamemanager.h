#pragma once

#include "ESAT/input.h"
#include "ESAT/window.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "ESAT/sprite.h"

#include <stdio.h>
#include <time.h>
#include <vector>
#include <memory>

#include "types.h"
//#include "agent.h"
class Agent;


class GameManager
{
private:
	static GameManager *instance_;
	GameManager();
	~GameManager();
public:
	
	static GameManager* Instance() {
		if (!instance_)
		{
			instance_ = new GameManager();
		}
		
		return instance_;
	}
	void Init();
	
	//step in ms, 40 ms = 25 fps
	const int IA_fixedStep = 40; 
	
	double ct;
	float Draw_time;
	bool exit;
	
	Agent* agent_;

};
