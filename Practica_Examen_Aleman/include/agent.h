#pragma once

#include "types.h"
#include "gamemanager.h"

class Agent {
public:
	Agent();
	~Agent();

	int id_;
	Point2 pos_;

	enum StateType {
		kIdle,
		kPatrol,
		kChase,
		kAtack,
		kSearch,
		kDead
	};
	StateType state_type_;
	
	void Init(int id,float x = 0, float y = 0);
	void Update(int step);
	void Mind(int step);
	void Body(int step);
	void Draw();

private:

	bool see_, hear_, dead_;
	float idle_time_, patrol_time_;
	double actual_time_;
	int max_time_idle_,max_time_patrol_;
	int atack_distance_, chase_distance_,dist_;
	int enemy_life_;
	
	void Idle(int step);
	void Patrol();
	void Chase();
	void Atack();
	void Search();
	void Dead();
	
protected:



};




