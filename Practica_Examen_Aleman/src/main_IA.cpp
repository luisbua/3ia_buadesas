#include "types.h"
#include "gamemanager.h"
#include "agent.h"
#include <stdlib.h>

namespace ESAT {
	
	///////Bucle del START///////

	void Imput()
	{
		GameManager* gm_ = GameManager::Instance();
		if (IsSpecialKeyDown(kSpecialKey_Escape))
		{
			gm_->exit = true;
		}
	
	}
	void Update(int step)
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->agent_->Update(step);
						
	}
	void Draw()
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->agent_->Draw();
	}

	///////Funciones principales////////////

	void Init()
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->agent_ = new Agent;
		gm_->agent_->Init(1);

 	}
	void Start()
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->ct = Time();
		srand(time(NULL));
		
		while (!gm_->exit)
		{
			Imput();
			float accutime = Time() - gm_->ct;
			
			while (accutime >= gm_->IA_fixedStep)
			{
				Update(gm_->IA_fixedStep);
				gm_->ct += gm_->IA_fixedStep;
				accutime = Time() - gm_->ct;
			}
			Draw();
		}
	}
	void ShutDown()
	{
		GameManager* gm_ = GameManager::Instance();
	}

	//////////////Main//////////////
	int ESAT::main(int argc, char **argv)
	{
		GameManager* gm_ = GameManager::Instance();
		gm_->Init();
				
		Init(); 
		Start();
		ShutDown();
			
		return 0;
	}
}//end namespace ESAT